---
id: 20210527094612
tags: []
references:
 - 20210108111506
---

# Terminal

## Terminal emulator

- [Alacritty](20220128093712)

## Terminal multiplexer

- [TMux](20210622095945)

## Utilities

- [Fig](https://fig.io/) : VSCode-style autocomplete to your existing terminal
