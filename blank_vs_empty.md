---
id: 20210716102142
tags: []
references:
  - 20210716102115
---

# Blank vs Empty

- In **both** cases the string
  - isn't null/undefined/...
  - has at least 1 character
- **Blank** : The string may composed by a whitespaces

  ```java
  StringUtils.isBlank(null)      = true
  StringUtils.isBlank("")        = true
  StringUtils.isBlank(" ")       = true
  StringUtils.isBlank("bob")     = false
  StringUtils.isBlank("  bob  ") = false
  ```

- **Empty** : The string has at least 1 character different from whitespace

  ```java
  StringUtils.isEmpty(null)      = true
  StringUtils.isEmpty("")        = true
  StringUtils.isEmpty(" ")       = false
  StringUtils.isEmpty("bob")     = false
  StringUtils.isEmpty("  bob  ") = false
  ```
