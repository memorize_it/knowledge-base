---
id: 20211215095019
tags: []
references:
 - 20210325170539
---

# How to check null in Java

- In custom functions, we should privilge the use of `object==null`
- In lambdas instead, we should use the `Objects::isNull`

## Explanation

- `Objects.isNull()` will add a new call but the behaviour will be the same as the implementation is :

```java
public static boolean isNull(Object obj) {
    return obj == null;
}
```

- The use of a function will make harder the flow analysis (depends on the tool), while checking for `null` is almost always supported
- Adding `(x)-> x == null` will createa new function that will do exaclty the same that `Objects::isNull`
