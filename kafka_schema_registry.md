---
id: 20211125145701
tags: []
references:
 - 20211125093626
---

# Kafka Schema Registry

- The schema will store schemas
  - This creates a single point of failure
  - Supports many languages
    - [Avro](20211125150234)
    - Json schema
    - Parquet
    - Orc
- [Producers](20211125103337) will push schemas
  - We will provide schema registry address on properties
  - We will push associated schema on first message
- [Consumers](20211125104304) will get schemas
  - Will ask if schema is updated before reading
- Supported operations :
  - CRUD
  - On the keys and on the values
- This allows to create a contract between producers and consummers

## Avro message interpretation

- Every message has ;
  - Magical byte (1 byte)
  - Schema id (4 bytes)
  - Content (rest of the message)
- The schema lives in the registry not in every message

## Java

```java
// Schema registry url should be provided in order to use it on consumer and producer configuations
properties.put("schema.registry.url", schemaRegistryServer);

// Serializer and deserializers should be provided
properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());

// We also need to tell Kafka
 consummer that we are not receiving generic avro messages
properties.put("specific.avro.reader", "true");
```
