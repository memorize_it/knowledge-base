---
id: 20210407154750
tags:
    - talk
    - react
    - youtube
references: []
---
# Refactoring React - Siddharth Kshetrapal

Watch it in [YouTube](https://www.youtube.com/watch?v=2Dw8gA60d_k).

Tips :

- Name behaviours as props, not interactions
  - Switch will toggle, not click -> onToggle
- Minimize the API surface area
  - Avoid adding new props, keep consistent on the codebase
  - If our input has onChange -> our toggle will have onChange instead of onToggle
- Every component must manage it internal components
  - Don't propagate a form submit to parent, stop and just call the onSubmit
- Spread props on components -> `{...this.props}`
  - It will allow to add properties to children without updating the component
  - If the spread is **after** -> user will override ours
  - If the spread is **before** -> we will override users
  - It may be a good idea to spread between the inputs and the outputs
- Put form values on state or access the form properties, don't do both
- SingleResponsabilityPrinciple : one component -> one thing
  - Create higher order components

    ```js
    return class Component extends React.Component {
        ...
    }

    function addSmartness(component){
        return class SmartComponent extends React.Component {
            constructor (props){
                super(props);

                this.state = {
                    disabled: true;
                }
            }

            onChange = (event) => {
                const disabled = event.target.value.length === 0;
                this.setState({disabled})
            }

            render(){
                return <Component {...this.props} disabled={this.state.disabled}></Component>
            }
        }
    }
    export default addSmartness(Component);
    ```

  - Or ... **UseHooks**

    ```js
    function useSmartness (defaultState){
        const [disabled, setDisabled] = React.useState(defaultState);

        onChange = (event) => {
            const disabled = event.target.value.length === 0;
            setDisabled({disabled})
        }

        return [disabled, onChange];
    }

    return class Component extends React.Component {
        const [disabled, onChange] = useSmartness(true);
    }
    ```

- Write tests before a refactor => verify behavior
- When a component has a single prop and has no children => use {props.children} in the child body instead
  - It will allow us to render text, a component... inside the child component without updating the code
  - We will avoid this if we are sure of the type of the props and we want to remove that flexibility
- If we need to apply a context to all the childrens of a component => use context

    ```js
    const FormContext = React.useContext({})

    ...

    return (
        <form>
            <FormContext.provider value={{disabled: props.disabled}}>
                <props.children>
            </FormContext.provider>
        </form>
    )
    ```
