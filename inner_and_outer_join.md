---
id: 20210114194433
tags: []
references: []
---
# Inner and Outer join

## What is Inner Join?

Only the rows that have matching values in both the tables.

## What is Outer Join?

Matching rows as well as some of the non-matching rows between the two tables. It handles the false match condition.

There are 3 types of Outer Join:

- Left Outer Join: Returns all the rows from the LEFT table and matching records between both the tables.
- Right Outer Join: Returns all the rows from the RIGHT table and matching records between both the tables.
- Full Outer Join: It combines the result of the Left Outer Join and Right Outer Join.
