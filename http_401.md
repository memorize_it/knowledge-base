---
id: 20210712153919
tags: []
references:
 - 20210712153753
---

# HTTP 401 - Unauthorized

See [RFC 7235](https://datatracker.ietf.org/doc/html/rfc7235#section-3.1)

    The 401 (Unauthorized) status code indicates that the request has not been applied because it lacks valid authentication credentials for the target resource.
