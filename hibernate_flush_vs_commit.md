---
id: 20210402085142
tags: []
references: []
---
# Hibernate flush vs commit

**Flush** shyncrhonizes with the persistent memory storage (it will add the **modifications to the transaction**).
**Commit** will commit the transaction to the **database**.
