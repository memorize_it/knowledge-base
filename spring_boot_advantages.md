---
id: 20210325165254
tags: []
references: []
---
# Spring boot advantages

- It provides a lot of default configurations.
- It comes with embedded tomcat or jetty server, so you don’t have to deploy jar.
- It reduces development code by avoiding a lot of boilerplate code.
- It increases productivity, add value faster.
- It provides starter projets.
