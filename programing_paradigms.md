---
id: 20210413103238
tags: []
references: []
---
# Programing paradigms

- [Functional programming](20210413103144)
- Object-oriented
- Procedural
- Event-driven
