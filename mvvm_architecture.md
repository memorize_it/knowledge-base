---
id: 20210412144953
tags:
    - angular
references: []
---
# MVVM architecture

- Model : data and the BL of the app.
- View : visual layer of application. HTML template.
- ViewModel : bridge between view and model.
