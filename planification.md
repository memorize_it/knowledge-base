---
id: 20210701143002
tags: []
references: []
---

# Planification

Tips on how to :

- correctly budget time and make estimations that are as close as possible to actual needs.
- identify what is interessting to focus on

## Priorisation

- Define **global objectives** for the project, and link to them every requests
- Get existing **KPI** that **justify** every **request**
- Set new KPI to define what is expected, and be able to evaluate if objectives are achieved

## Stats

- Keep track of historical data, check it before planning

## Many teams

- Budget time to help other teams working on common subjects
- Create a temporary **squad**, project focused, with members from **different teams**
  - optimize communication
  - increase knowledge sharing
  - allow internal mobility
