---
id: 20210402083146
tags: []
references: []
---
# Spring transactions

Source : [Baeldung](https://www.baeldung.com/spring-transactional-propagation-isolation)

Use @Transactional annotation.

Transaction propagation :

- REQUIRED (Default): create transaction if doesn't exist, otherwise join the active transaction.
- SUPPORTS : use a transaction if already exists.
- MANDATORY : if transaction active, join, else throw an exception.
- NEVER : if transaction active throw an exception.
- NOT_SUPPORTED : if transaction active, suspend, then execute without transaction.
- REQUIRES_NEW : suspend current transaction and create a new one.
- NESTED : if transaction active, mark savepoint, thyen if the method fails, transaction rollbacks to the savepoint.

Transaction isolation :

- Spring defaults to the RDBMS isolation level.
  - If the DB changes, Spring too.
- Each isolation level prevents zero or more concurrency side effects on a transaction:
  - Dirty read : read the uncommitted change of a concurrent transaction
  - Nonrepeatable read : get different value on re-read of a row if a concurrent transaction updates the same row and commits
  - Phantom read : get different rows after re-execution of a range query if another transaction adds or removes some rows in the range and commits

- READ_UNCOMMITTED : we read from other transactions.
- READ_COMMITTED : prevents dirty read. Won't see uncommited changes.
- REPEATABLE_READ : prevents dirty and no repetable reads. Not affected by uncommited changes.
  - Avoids lost update : two transactions over the same row, overriding.
  - Default on MySQL
  - Not supported by Oracle
- SERIALIZABLE : prevents all concurrency, but slower.
