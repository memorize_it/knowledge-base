---
id: 20220406094005
tags: []
references:
 - 20210930142716
---

# Gwern Branwen's blog

Gwern Branwen's [blog](https://www.gwern.net/index) has a minimalist design.

This website allows for a popover preview of articles on hover which allows to easily navigate on the content without losing track.

Deisgn decisions are explained in this [post](https://www.gwern.net/Design).

## Screen-shots

![Popover](./images/gwern_branwen_popover.png)
![Popover lock](./images/gwern_branwen_popover_lock.png)
![Popover locked](./images/gwern_branwen_popover_locked.png)
![Sidenote](./images/gwern_branwen_sidenotes.png)

## What do I like

- **Minimalist**
- Preview on hover
- Side note previews
