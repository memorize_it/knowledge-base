---
id: 20210608121248
tags: []
references:
 - 20210108162425
---

# Linux

- [Wifi from cli](20200913130033)
- [Gnome autofocus windows](20210317120558)
- [Gnome system monitor](20210322134245)
- [Kill process listening on port](20210608120045)
- [Install kubectl](20210614171336)
- [Install Vault](20210614171849)
- [Add resolution to screen](20210716094527)
- [Increase file watchers](20210824103704)
- [Clean cache](20220307165913)
- [Kernel version](20220310085415)
- [Get file encoding](20220520102016)
