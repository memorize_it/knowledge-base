---
id: 20220131144121
tags: []
references:
 - 20210608121348
---

# Migrate sql db entries without overriding latest

One way to achieve this is to run two queries.

- One will copy all the versions of a given entry to the latest, using an `INSERT` with an `ON CONFLICT DO NOTHING` clause, avoiding errors if an entry exisit that already fills the constraint.
- Another will delete all the previous entries.
