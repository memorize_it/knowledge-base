---
id: 20220316104903
tags: []
references:
 - 20210608121324
---

# Keycloak

- Access management software
- 2013 by #RedHat developers
- RBAC : Role based access control

## Functionalities

- SSO
- Can use an identity broker (OpenID, SAML...)
  - This decouples the application from the identity provider
- Integrate social login providers
- Connectors to allow federation
- Authorization service

## Components

- Realms : Users, Clients (Applications), Roles, Groups
  - One instance N realms
