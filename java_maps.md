---
id: 20210401093636
tags: []
references: []
---
# Java Maps

- Can be traversed :
  - All the keys
  - All the values
  - For a given pair of key/value

## Interfaces

- SortedMap
- NavigableMap
- ConcurrentMap
- ConcurrentNavigableMap

## Implementations

- TreeMap : not synchornized
  - Ordered
- HashTable : thread-safe
  - Not null as key
  - Two parameters for performance :
    - Initial capacity : number of elemenbts
    - Load factor (default 0.75) : amount of space used before increasing size
- HashMap: not synchronized
  - Null can be key
- ConcurrentHashMap
  - Like HashTable but more efficient on concurrent readings
- EnumMap : not synchronized
  - Values must belong to same enum
