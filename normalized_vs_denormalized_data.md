---
id: 20210628111414
tags: []
references:
 - 20210608121348
---

# Normalized vs Denormalized data

Normalized : Data schema avoids data duplication.
Denormalized : Data may be duplicated.
