---
id: 20210401091846
tags: []
references: []
---
# Collection types

## Lists

- Ordered => Index access
- Double accepted

- [Java Lists](20210401092130)

## Sets

- No direct access
- No double => Single null

- [Java sets](20210401092721)

## Maps

- Assiciations between entities
- Unique key, but different keys may point to same value
- One key may be associated to many elements
- Single null key, many null values

- [Java Maps](20210401093636)

## Queues

- Ordered
- Items must be processed in order
- May be FIFO or LIFO
  
- [Java queues](20210401094433)
