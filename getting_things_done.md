---
id: 20210123083901
tags: []
references: []
---
# Getting things done

## Getting things done basics

Objective: A system that I trust to asses, capture, realize and remember everything in my mind.

I need to:

- Through the day, capture anything I need to remember
- Transform these toughts into actions, or reject otherwise
- Classify them in four categories:
  - Next actions: Things to do soon
  - Waiting for: things that do not depend on me, must  specify the interlocutor, the start date and deadline (if known)
  - Projects: long term thematic classifications, or if from a single tought there came more than a single action
  - Projects must be :
    - self descrtiptive
    - Have a specific target
  - Some day/Maybe : personal archive

### Capture

- If it can be captured as an idea : write it down
- If it needs something physical : classify it on folders
- If it needs a contact add it to the contacts section

### Next actions

Next actions can be inside 3 categories:

- Can be done in less than 3 minutes ? : JUST DO IT !
- There is a specific deadline (day and/or time) : **calendar**
- Otherwise : Goes to next actions, which must be done in order

Next actions can be composed of several lists, depending on the context :

- where an action can be done: @city @work @computer @everywhere
- a person it's related to : @bob @alice

If I want to track some of these contexts I can do it throught the week scheduler

## My GTD

### What and where

- Note taking : where all the notes are written, befor classifying them
- Tracking through time : A visual shchedule of current week
- Projects :Easy to access objectif and upcoming tasks of  current projects
- Storage : a place to store files, notes, tickets... that need to be classified
  - Unproccessed storage
    - 1 single place where every new document goes
    - If the document has a related date note it close to it
  - Upcoming storage: with one year overview:
    - 12 folders, one for every month
    - 4 folders, one for each of the next 4 weeks
  - Current storage:
    - 7 folders, one for every day of the week
  - In total 24 folders or containers

### Daily review

- Open current days storage
  - take what you need
  - note time if related actions (travels...)
- Check current day no time actions
  - fix time to actions
- Check weekly tasks
- Classify unprocessed storage which can be fastly classified

### Weekly review

Inputs :

- Fast input:
  - Current week notes
  - Unproccessed storage
- Processed information :
  - Projects
  - Upcoming storage
- Permanent utils:
  - Trigger list
  - Weekly/Daily must do (Default week planning)
- Empty week schedule
- Empty week storage

Outputs:

- Full Week schedule

#### Procedure

- Go through all the new notes and classify them
- Check unfinished task from previous week
- Go through the triggers list to complete with new actions
- Review unprocessed storage
  - create actions
  - classify materials into upcoming storage
- Review upcoming storage for next week
  - create actions
  - classify the content in current storage
- Place actions in the week scheduler:
  - Next actions
    - Monday-sunday schedule with time/day tasks to be done
    - Contexts are differenciated with colors/symbols
    - If not possible to know, they will be on week tasks
    - If files/papers... are attached put them in the related current storage folders
  - Waiting for:
    - Like Next actions
  - Projects:
    - Choose at least one action per project
  - Others:
    - The contacts required by next actions
    - Setup global trackers for the context you want to improve
- Review storage :
  - Replace the upcoming storage for  the next 3 weeks
  - Recheck the next month storage and setup if something required on the  4th week

## Implementation

### Weekly schedule

- Header: year and week number
- First page: 2 (Time specific - Day specific) x 7 (Monday-Sunday)
- Second page:
  - No fixed : Week tasks with checkboxes
  - No fixed : Waiting on with checkboxes
  - Contacts
  - Global  trackers : 5 contexts, one checkbox per week day

## Sources

- [GTD in 15 minutes - A Pragmatic Guide to Getting Things Done](https://hamberg.no/gtd/)
- [Bullet Journal + GTD for Maximum Productivity](https://www.bohoberry.com/bullet-journal-gtd/)
