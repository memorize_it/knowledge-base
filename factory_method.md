---
id: 20210114175221
tags:
    - design_pattern
references: []
---
# Factory method

A function that creates new objects, usually of the same interface, given some parameters.

The consumer doesn't need to know the differences between the implementations, or how to call the object creator.
