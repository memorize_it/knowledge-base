---
id: 20210329141549
tags: []
references: []
---
# Spring Data

## Configuration of the repo beans

```java
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("fr.ippon.repository.jpa")
@EnableMongoRepositories("fr.ippon.repository.mdb")
```

## Lighten the results

We can use a smaller interface than the base interface as a result.
