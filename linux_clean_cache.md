---
id: 20220307165913
tags: []
references:
 - 20210608121248
---

# Linux clean cache

`echo 3 > /proc/sys/vm/drop_caches`
