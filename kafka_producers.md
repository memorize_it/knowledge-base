---
id: 20211125103337
tags: []
references:
 - 20211125093626
---

# Kafka Producers

- Creates messages in n topics
- On configuration you provide a list of brokers
  - If you add a new broker you must update your producers
- Communicates with [brokers](20211125105813)
- #Tcp protocol
- May be asynchronous
  - you call it x times but only pushes every y time (gorups messages)
- Can retry

## [Guarantees](20211125111933)

- acks = 0 : at most once
- acks = 1 : at least once
- enable.idempotence = true : exaclty once

## Cli

```bash
# kafka-console-producer --broker-list <BROKER_LIST> --topic <TOPIC_NAME>
kafka-console-producer
kafka-console-producer --broker-list 127.0.0.1:9092 --topic my_topic
```

## Java

```java
import org.apache.kafka.clients.producer.*;

// Min properties
Properties properties = new Properties();
properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

// Create
KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

// Setup
String topic = "cart-topic";
String key = "key";
String value = "value";

// Create record
ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);

// Send data to Kafka
producer.send(record, (recordMetadata, e) -> {
    logger.info(String.format("Record sent %s - %s", key, value));
});

// Flush data and close producer
producer.flush();
producer.close();
```
