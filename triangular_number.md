---
id: 20211207135629
tags: []
references:
  - 20211207133524
---

# Triangular number

The n-th triangular number is the number of dots in the triangular arrangement with n dots on each side, and is equal to the sum of the n natural numbers from 1 to n.

It can be calculated for n with the following formula : `(n*(n+1))/2`

```js
function triangularNumber(n) {
  return (n * (n + 1)) / 2;
}
```

See [Wikipedia](https://en.wikipedia.org/wiki/Triangular_number)
