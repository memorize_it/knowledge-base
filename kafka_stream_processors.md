---
id: 20211125103644
tags: []
references:
 - 20211125093626
---

# Kafka Stream processors

- Ways to consumme messages
- Aggregates and merges messages to produce new messages

## Stream processor frameworks

### Kafka streams

- Stateless transformations : functional processing, only the message is required
- Stateful transformations : state is used
- Exactly one guarantee
- Input messages are processed by a graph of processors
- Output messages are published by another producer
- State can be stored in a database (RocksDb)

- KStream
  - Apply a transformation with new value and latest generated value
- KTable
  - Upsert based on key
  - A changelog, latest value recorded
  - Table is locale, one by partition
- GlobalKTable
  - Like KTable but one for topic

- Only exists in Java / Scala (kafka-stream library)

### Others

- [Spark](https://spark.apache.org)
- [Storm](https://storm.apache.org/)
- [Flink](https://flink.apache.org/)
