---
id: 20210608121324
tags: []
references:
  - 20210108162425
---

# App development

## Web

- [Optimize lighthouse score](20210109140158)
- [Observables vs Promises](20210412150009)
- [JAM Stack](20210414103545)
- [HTTP](20210712153753)
- [Webpack](20210824104002)
- [CORS](20220316092722)
- [Reactive programming](20220316141406)

## Services

- [Keycloak](20220316104903)

## Javascript

- [Duck typing](20221013145032)
- [Array of length n](20221013145033)
- [Prototype pollution](20221115113929)
- [XState](20221121164203)

## Typescript

- [Narrow down types](20221004115911)

## Redux

- [Redux](20210412153110)
- [Redux talks](20210413145323)

## RxJS

- [RxJs](20210414104530)
- [Learn RxJs](20210414104725)

## React

- [React PWA setup](20210108163102)
- [Mooc : React-Native](20210311080507)
- [React talks](20210407154536)

## Angular

- [Angular life cycle hooks](20210412144416)
- [MVVM architecture](20210412144953)
- [AOT Compilation](20210412145302)
- [Angular directives](20210412150245)
- [Angular talks](20210412170324)
- [Angular design patterns](20210413154453)
- [Design patters used on Angular](20210412145533)

## Next.js

- [Nextjs with react-helmet](20210110122750)

## Mobile

- [Mobile app development strategies](20210414121055)
- [Point app inside emulator to localhost server](20210316112450)
- [Fastlane](20210317112642)

## Style

- [Atomic design](20220316164119)
- [Tailwind CSS](20220316153935)
