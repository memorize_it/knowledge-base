---
id: 20210329144402
tags: []
references: []
---
# Spring security

Spring makes use of the **DelegatingFilterProxy** for implementing security.

It is a Proxy for standard Servlet Filter, delegating to a Spring-managed bean that implements the Filter interface
Its the starting point in the springSecurityFilterChain which instantiates the Spring Security filters according to the Spring configuration.
