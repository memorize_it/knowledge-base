---
id: 20210325165609
tags: []
references: []
---
# Spring boot Application annotation

```java
@SpringBootApplication
```

Is the equivalent of :

```java
@Configuration
@EnableAutoConfiguration
@ComponentScan
```
