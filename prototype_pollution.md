---
id: 20221115113929
tags: []
references:
  - 20210608121324
---

# Prototype pollution

## Definition

Prototype pollution consist on overloading the original object prototypes,
therefore impacting most of the objects in the runtime.
This happens usually on `Object` or `Array` objects.

Most of the object or array instances will access methods defined by their parents,
using their `__proto__` or `prototype` properties.

## Why this may be dangerous

- We can break our JS runtime
- In some cases this may be a security issue: a user may exploit this in order to access another users runtime

## Examples

Usually :

```javascript
const a = { a: "a" };
a.toString(); // [object Object]

// But Object may be overloaded in order to impact other objects
const a = { a: "a" };
Object.prototype.toString = () => "Polluted !";
a.toString(); // I am an Object
```

The general context will remain polluted

```javascript
// "Normal JS object before anything"
const jsObject = { a: "a" };
obj.toString();

// Some key with the code __proto__ was added to an object
const USER_VALUE_CAUSING_POLLUTION = "__proto__";
const newObject = {};
newObject[USER_VALUE_CAUSING_POLLUTION].toString = () => {
  return "polluted by __proto__";
};
newObject.toString();

// The toString of an object creater later remains polluted
const anotherObject = {};
anotherObject.toString();
```

### In `.map` and `.reduce` methods

```javascript
const USER_VALUE_CAUSING_POLLUTION = "__proto__";
const COLORS = ["red", USER_VALUE_CAUSING_POLLUTION, "pink"];
const arrayByColor = {};

// Before my accumulator is not an array
arrayByColor.length; // undefined
JSON.stringify(arrayByColor); // '{}'

COLORS.reduce((acc, color) => {
  acc[color] = [];
  return acc;
}, arrayByColor);

// After my accumulator __proto__ is an array
arrayByColor.length; // 0

// But it remains an object
typeof arrayByColor; // 'object'
Array.isArray(array); // false

// and in any case: it has not an entry for __proto__
JSON.stringify(arrayByColor); // {"red":[],"pink":[]}
```

### In property check

This is not directly related to the pollution, but remains a side effect of incorreclty accesing uncontrolled properties

```javascript
const load = (loaders, code) => {
  loaders[code] = true;
};
const isLoading = (loaders, code) => !!loaders[code];

const loaders = {};

load(loaders, "a");

isLoading(loaders, "a"); // true
isLoading(loaders, "b"); // false
isLoading(loaders, "constructor"); // true !
```

## How to avoid this issue

### In `.map` and `.reduce` methods

We should instead use maps when the keys are :

- unknown / user defined
- dynamics: we do not know the values in advance

This has some advantages :

- Maps have increased key access [performance](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map#objects_vs._maps)
- Maps are iterables
  - We can check the amount of keys just using: `map.size`
- We avoid prototype pollution

Some disadvantages:

- We do not serialize a map in the same way as an object, we need to make some extra work

```javascript
const USER_VALUE_CAUSING_POLLUTION = "__proto__";
const COLORS = ["red", USER_VALUE_CAUSING_POLLUTION, "pink"];

// My accumulator is a map
const arrayByColorsFixed = COLORS.reduce((acc, color) => {
  acc.set(color,  []);
  return acc;
}, new Map());

// But... it's not straightforward to serialise it
JSON.stringify(arrayByColorsFixed); // {}

// But it will contain all the values if properly done
JSON.stringify(Object.fromEntries(arrayByBrandFixed.entries())); // {"red":[],"__proto__":[],"pink":[]}

// And we have not polluted the rest of our context
{}.length; // undefined
```

### In property check

```js
const load = (loaders, code) => {
  loaders[code] = true;
};
const isLoading = (loaders, code) =>
  loaders.hasOwnProperty(code) && !!loaders[code];

const loaders = {};

load(loaders, "a");

isLoading(loaders, "a"); // true
isLoading(loaders, "b"); // false
isLoading(loaders, "constructor"); // false !
```

## Another solution

We can just freeze the prototypes :

```javascript
Object.freeze(Object.prototype);
Object.freeze(Object.constructor);
```

## Sources

- [JavaScript prototype pollution attack in NodeJS](https://github.com/HoLyVieR/prototype-pollution-nsec18/blob/master/paper/JavaScript_prototype_pollution_attack_in_NodeJS.pdf)
- [Vulnerable server example](https://github.com/Kirill89/prototype-pollution-explained)
