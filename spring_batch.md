---
id: 20220523171457
tags: []
references:
 - 20220512154154
---

# Spring batch

- [Transactions in spring batch jobs](20220523171640)
- [Skip and retry policies](20220523171641)
