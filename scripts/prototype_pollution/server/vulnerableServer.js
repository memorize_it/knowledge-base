const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const _ = require("lodash");

// This code is a simplified version of the example available at : https://github.com/Kirill89/prototype-pollution-explained

// This is a simple chat API:
//
// - All users can see all messages.
// - Registered users can post messages.
// - Administrators can delete messages.
//

const app = express();

///////////////////////////////////////////////////////////////////////////////
// In order of simplicity we are not using any database.
const users = [
  { name: "user" },
  { name: "hacker" },
  { name: "admin", canDelete: true },
];

let messages = [];
let lastId = 1;

function findUser(username) {
  return users.find(({ name }) => name === username);
}
///////////////////////////////////////////////////////////////////////////////

app.use(bodyParser.json());

app.use(cors());

// Get all messages (publicly available).
app.get("/", (_, res) => {
  res.send(messages);
});

// Post message (restricted for users only).
app.put("/", (req, res) => {
  const user = findUser(req.body.username || "");

  if (!user) {
    res.status(403).send({ ok: false, error: "Access denied" });
    return;
  }

  const message = {
    // Default message icon. Cen be overwritten by user.
    icon: "👋",
  };

  _.merge(message, req.body.message, {
    messageId: lastId++,
    timestamp: Date.now(),
    username: user.name,
  });

  messages.push(message);
  res.send({ ok: true });
});

// Delete message by ID (restricted for users with flag "canDelete" only).
app.post("/delete", (req, res) => {
  const user = findUser(req.body.username);

  if (!user || !user.canDelete) {
    res.status(403).send({ ok: false, error: "Access denied" });
    return;
  }

  messages = messages.filter((m) => m.messageId !== req.body.messageId);
  res.send({ ok: true });
});

app.listen(3001);
