import UserView from "../components/UserView";
import { users } from "./data";

const User = () => {
  return <UserView user={users.user} messages={[]}></UserView>;
};

export default User;
