export interface User {
  name: string;
  username:string;
  profilePicture: string;
}

export const users: Record<string, User> = {
  user: {
    name: "Mimi",
    username: "user",
    profilePicture:
      "https://images.pexels.com/photos/2071873/pexels-photo-2071873.jpeg?auto=compress&cs=tinysrgb&w=150&h=150&dpr=1",
  },
  hacker: {
    name: "Darth Cator",
    username: "hacker",
    profilePicture:
      "https://images.pexels.com/photos/96938/pexels-photo-96938.jpeg?auto=compress&cs=tinysrgb&w=150&h=150&dpr=1",
  },
  admin: {
    name: "Mr Grumpy",
    username: "admin",
    profilePicture:
      "https://images.pexels.com/photos/208984/pexels-photo-208984.jpeg?auto=compress&cs=tinysrgb&w=150&h=150&dpr=1",
  },
};
