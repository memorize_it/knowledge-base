import Image from "next/image";
import Link from "next/link";
import { users } from "./data";

export default function Home() {
  return (
    <ul className="flex flex-row items-center justify-center h-screen">
      {Object.values(users).map(({ profilePicture, name, username }, i) => {
        return (
          <Link href={username} key={i}>
            <div className="items-center flex-start m-10">
              <Image
                src={profilePicture}
                alt=""
                className="object-none w-24 h-24 mb-3 rounded-full shadow-lg"
                width="750"
                height="750"
              />
              <h5 className="mb-1 text-xl font-medium text-gray-900 dark:text-white text-center">
                {name}
              </h5>
              <p className="text-sm text-gray-500 dark:text-gray-400 text-center">
                {username}
              </p>
            </div>
          </Link>
        );
      })}
    </ul>
  );
}
