import UserView from "../components/UserView";
import { users } from "./data";

const Hacker = ()=> {
    return (
        <UserView user={users.hacker} messages={[]}></UserView>
    )
}

export default Hacker;