import UserView from "../components/UserView";
import { users } from "./data";

const Admin = ()=> {
    return (
        <UserView user={users.admin} messages={[]}></UserView>
    )
}

export default Admin;