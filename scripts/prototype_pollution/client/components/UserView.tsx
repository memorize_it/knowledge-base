import Image from "next/image";
import { useCallback, useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import {
  deleteMessage,
  getMessages,
  sendMessage,
  sendPayload,
} from "../api/messagesApi";
import { User } from "../pages/data";

interface Message {
  text: string;
  messageId: string;
  sender: User;
}

interface UserViewProps {
  user: User;
  messages: Message[];
}

interface MessageForm {
  message: string;
}

export default function UserView({ user }: UserViewProps) {
  const { name, profilePicture, username } = user;
  const [messages, setMessages] = useState<Message[]>([]);

  const getApiMessages = useCallback(() => {
    getMessages().then(setMessages);
  }, []);

  const deleteApiMessage = useCallback(
    (messageId: string) => {
      deleteMessage(user, messageId);
    },
    [user]
  );

  const sendApiPayload = useCallback(() => {
    sendPayload(user);
  }, [user]);

  useEffect(() => {
    getApiMessages();

    const interval = setInterval(() => {
      getApiMessages();
    }, 500);

    return () => clearInterval(interval);
  }, [getApiMessages]);

  const { register, handleSubmit, reset } = useForm<MessageForm>();

  const onSubmit: SubmitHandler<MessageForm> = async ({ message }) => {
    await sendMessage(user, message);
    reset();
  };

  return (
    <div className="flex flex-row items-center justify-center h-screen">
      <div className="items-center flex-start m-10">
        <Image
          src={profilePicture}
          alt=""
          className="object-none w-24 h-24 mb-3 rounded-full shadow-lg"
          width="750"
          height="750"
        />
        <h2 className="mb-1 text-xl font-medium text-gray-900 dark:text-white">
          {name}
        </h2>
        <span className="text-sm text-gray-500 dark:text-gray-400">
          {username}
        </span>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mb-6">
            <label
              htmlFor="message"
              className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
            >
              Your message
            </label>
            <textarea
              id="message"
              rows={4}
              className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Wow ..."
              {...register("message")}
            ></textarea>
          </div>
          <button
            type="submit"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 mb-2"
          >
            Send message
          </button>
        </form>
        {username === "hacker" && (
          <button
            type="button"
            className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
            onClick={() => sendApiPayload()}
          >
            Send payload !
          </button>
        )}
      </div>

      <div className="w-full h-screen p-16">
        <h2 className="mb-1 text-xl font-medium text-gray-900 dark:text-white">
          Messages
        </h2>
        <ul>
          {messages.map(
            (
              {
                text,
                sender: { profilePicture: senderPicture, name: senderName },
                messageId,
              },
              i
            ) => {
              return (
                <li key={i} className="w-100">
                  <div className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
                    <div className="flex">
                      <Image
                        src={senderPicture}
                        alt=""
                        className="object-none w-16 h-16 mb-3 rounded-full shadow-lg"
                        width="750"
                        height="750"
                      />
                      <p>{senderName}</p>
                    </div>
                    <p className="font-normal text-gray-700 dark:text-gray-400">
                      {text}
                    </p>
                    <button
                      type="button"
                      className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                      onClick={() => deleteApiMessage(messageId)}
                    >
                      Delete
                    </button>
                  </div>
                </li>
              );
            }
          )}
        </ul>
      </div>
    </div>
  );
}
