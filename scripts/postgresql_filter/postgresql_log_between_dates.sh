LOG_FILE=$1;
START_DATE=$2;
END_DATE=$3;

UNIX_START_DATE=`date -d "$START_DATE"`;
UNIX_END_DATE=`date -d "$END_DATE"`;

awk -v start="$UNIX_START_DATE" -v end="$UNIX_END_DATE" '{
    dunix ="date -d \""$1" "$2" "$3"\""
    dunix | getline var
    if (var < end && var > start){
        print $0
    }
}' $LOG_FILE;
