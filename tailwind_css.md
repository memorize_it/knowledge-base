---
id: 20220316153935
tags: []
references:
 - 20210608121324
---

# Tailwind CSS

## Basic principles

CSS Without leaving HTML.
Utilitary low level classes.
One class one CSS attribute.

## Advantages

Enables consistence over components and different developers.
Integrates with different Front frameworks.
Opinionated default style. We can extend and override these defaults using a js export.
