---
id: 20210117094456
tags: []
references: []
---
# Test levels

- Unit tests : Test just one file/class/component at a time. Mock ALL the dependencies.
- Integration tests : Test the interactions between the different components. Don't mock any local class.
- End-to-end tests : Test the whole environnment (front-end, back-end, data). Use protractor or similar framework.
