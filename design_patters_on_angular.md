---
id: 20210412145533
tags:
    - design_pattern
    - angular
references: []
---
# Design patters used on Angular

- [Singleton](20210114174432) : services are instatianted once in the application life-cycle.
- [Observer](20210114175729) :
  - connections between view and controler. The VM layer will expose observers to the view.
