---
id: 20210412165330
tags: []
references: []
---
# Pure function

See [Wikipedia](https://en.wikipedia.org/wiki/Pure_function)

Function that has the followinf properties :

- Given same arguments, function will always return same output.
- Has no side effect, the function will not mutate local state variables, non-local variables or input/output)

Why to use pure functions :

- Easy to test, they won't use state, no need of mocks or spies
- Self-documenting, as they won't use state
- Concurrent, we can run them in parallel
- Cacheable, we can store the result for given arguments
