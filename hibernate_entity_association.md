---
id: 20210329102038
tags: []
references: []
---
# Hibernate entity association

- @OneToMany : on a field that refers to all the entities pointing to this entity.
  - We provide as a parameter the name of the field on the other entity
  - Fetch mode default to LAZY
- @ManyToOne : on a field that points to another entity group.
  - Fetch mode default to EAGER
  - It's a good idea to specify LAZY
- @OneToOne
  - Fetch mode default to EAGER
  - It's a good idea to specify LAZY

- Using **bi-directional** mappings has a sever **impact on performance**
- We must **manage bi directional relations** on **Entity level**, and no on service level.
- We must implement HashChode

- @ManyToMany : hibernate creates the join table
