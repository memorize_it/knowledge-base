---
id: 20220316140356
tags: []
references:
 - 20210325170539
---

# Spring WebFlux

Based in #Pivotal Project Reactor.

The Reactor context allows to share information when multiple threads manage the same request, sharing information that usually would be linked to the thread (security, correlation id...)
