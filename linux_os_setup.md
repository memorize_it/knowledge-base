---
id: 20210108111506
tags: []
references: []
---
# Linux OS setup

## Install the most used commands

```shell
sudo apt install curl zsh git xclip
```

## Install the tools

- [Oh my zsh](20210108111125)
- [Node.js](20210108112223)
- [Java setup](20210108115151)
- [Android developper tools](20210108113427)
- [Docker](20210108130555)
- [Terminal](20210527094612)

## Apps

- [BpyTop](https://www.funkyspacemonkey.com/bpytop-bashtop-port-written-in-python) : System monitoring
