---
id: 20210608120045
tags: []
references:
 - 20210608121248
---

# Kill process listening on port

```bash
kill $(lsof -t -i:[PORT])
```
