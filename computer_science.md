---
id: 20210608121335
tags: []
references:
 - 20210108162425
---

# Computer science

- [Design patterns](20210114174306)
- [Tests](20210816102242)
- [Gherkin tests](20210117101053)
- [SOLID](20210114192710)
- [ACID](20210329154501)
- [Collection types](20210401091846)
- [Pure function](20210412165330)
