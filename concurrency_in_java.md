---
id: 20220120115017
tags: []
references:
 - 20210325170539
---

# Concurrency in Java

## Tools

- [AtomicReference vs volatile](20220120114633)
- [Synchronized methods](20220121142159)
- [Double checked locking](20220121145106)
