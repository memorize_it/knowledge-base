---
id: 20210804094627
tags: []
references:
 - 20210108115151
---

# jEnv

jEnv is a command line tool to help you forget how to set the JAVA_HOME environment variable.

Check the [official website](https://www.jenv.be/) or the [GitHub](https://github.com/jenv/jenv) repository.

## Install

```bash
git clone https://github.com/jenv/jenv.git ~/.jenv
echo 'export PATH="$HOME/.jenv/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(jenv init -)"' >> ~/.zshrc
```

## Add jdk

```shell
jenv add $PATH
```

## Use

### List managed JDKs

```shell
jenv versions
```

### Configure global version

```shell
jenv global VERSION
```

### Configure local version (per directory)

```shell
jenv local VERSION
```

### Configure shell instance version

```shell
jenv shell VERSION
```
