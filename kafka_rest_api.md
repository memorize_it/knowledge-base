---
id: 20211125143406
tags: []
references:
 - 20211125093626
---

# Kafka Rest API

- Exposes kafka through a rest api.
- Json communication.
- We deploy an app that will expose api and perform tasks on the kafka cluster.

## Allows to

- CRUD Topics (paritions, replicas...)
- Produce/Consome messages
- ...

## Tradeoffs

- Less performant than consumer group approach.
