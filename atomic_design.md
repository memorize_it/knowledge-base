---
id: 20220316164119
tags: []
references: []
---

# Atomic design

- Quark: attributes for atoms, they can't live on their own but they can be shared by multiple atoms.
- Atom: single component, can't be separed
- Mollecule : atom composition
- Organism : Atoms and mollecules together, logical use
- Template : Organisms placed in the space
- Page : Implementation of templates
