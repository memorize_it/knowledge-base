---
id: 20210114192710
tags: []
references: []
---

# Solid

- Single-responsibility principle: There should be only one reason to change a class. To be true, the class can only do one thing.
- Open–closed principle: "Software entities ... should be open for extension, but closed for modification."
- Liskov substitution principle: "Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program.".
- Interface segregation principle: "Many client-specific interfaces are better than one general-purpose interface."
- Dependency inversion principle: One should "depend upon abstractions, [not] concretions."
