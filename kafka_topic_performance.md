---
id: 20211126154846
tags: []
references:
 - 20211125104038
---

# Kafka Topic performance and paritioning

More partitions means better throuyghput

But also :

- More file descriptors
- Increase downtime in case of rebalacing
- Can increase latency
- May require more memory on the client (one buffer per patition)
