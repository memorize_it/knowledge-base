---
id: 20210824103704
tags:
 - stack_overflow
references:
 - 20210608121248
---

# Increase file watchers

After an answer on [Stack Overflow](https://stackoverflow.com/a/55763478)

Linux uses the inotify package to observe filesystem events, individual files or directories. To increase max amount of files watched :

```bash
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```
