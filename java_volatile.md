---
id: 20220120113043
tags: []
references:
 - 20210325170539
---

# Java volatile

The Java `volatile` keyword allows to share a variable between multiple threads.

The variable read and write will use the memory shared between thread.

Another option is to use the [AtomicRefrence](20220120114435).
