---
id: 20210114174306
tags:
    - design_pattern
references: []
---

# Design patterns

- [Adapter](20220506153420)

## Creational design patterns

- [Singleton](20210114174432)
- [Factory](20210114175221)

## Structural design patterns

- [Facade](20210114190857)

## Behavioural design patterns

- [Observer](20210114175729)
- [Strategy](20210114190721)
