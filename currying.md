---
id: 20210413150128
tags: []
references: []
---
# Currying

- Take a function which takes N arguments
- Make a function which takes 1 argument
- Our function will return a function
- Then we separate our arguments with parenthesis

    ```js
    function add(a){
        return function(b){
            return a + b;
        }
    }

    add(1)(5);
    // or

    const add2 = a => b => a + b;
    ```
