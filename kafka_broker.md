---
id: 20211125105813
tags: []
references:
 - 20211125093626
---

# Kafka Broker

- A server running kafka
- Brokers form clusters
- Needs a quorum manager : Zookeeper or raft for example

## Role

- Write on disk
- Consumer / producer interface
- Choose leader
