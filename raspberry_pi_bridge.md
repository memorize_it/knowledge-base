---
id: 20200816104302
tags: []
references: []
---
# Raspberry PI Bridge

## Create the bridge

Source : [Setting up a Raspberry Pi as a bridged wireless access point](https://www.raspberrypi.org/documentation/configuration/wireless/access-point-bridged.md)

### Install the access point software

```bash
sudo apt install hostapd
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
```

### Setup the network bridge

#### Create a bridge device and populate the bridge

```bash
sudo nano /etc/systemd/network/bridge-br0.netdev
```

File contents:

```plaintext
[NetDev]
Name=br0
Kind=bridge
```

```bash
sudo nano /etc/systemd/network/br0-member-eth0.network
```

File contents:

```plaintext
[Match]
Name=eth0

[Network]
Bridge=br0
```

Now enable the systemd-networkd service to create and populate the bridge when your Raspberry Pi boots:

```bash
sudo systemctl enable systemd-networkd
```

#### Define the bridge device IP configuration

We need to block the eth0 and wlan0 interfaces from being processed, and let dhcpcd configure only br0 via DHCP.

```bash
sudo nano /etc/dhcpcd.conf
```

Add the following line near the beginning of the file (above the first interface xxx line, if any):

```plaintext
denyinterfaces wlan0 eth0
```

Go to the end of the file and add the following:

```plaintext
interface br0
```

### Ensure wireless operation

```bash
sudo rfkill unblock wlan
```

### Configure the access point software

```bash
sudo nano /etc/hostapd/hostapd.conf
```

```plaintext
country_code=GB
interface=wlan0
bridge=br0
ssid=NameOfNetwork
hw_mode=g
channel=7
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=AardvarkBadgerHedgehog
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
```

### Reboot

```bash
sudo systemctl reboot
```
