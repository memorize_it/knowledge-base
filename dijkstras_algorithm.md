---
id: 20211216141209
tags: []
references:
 - 20211207133524
---

# Dijkstra's algorithm

Provides with the shortest path in a weighted graph.

Preparation :

- Mark all nodes as unvisited. Create a set of all `unvisited nodes`.
- Assign to every node a `tentative distance` of infinite.
- Assign to the starting node a `tentative distance` of zero.
- The current node is the starting node.

Iteration (For the current node):

- For every neighbour in the `unvisited nodes` set.
  - Add the `tentative distance` of the current node to the edge connecting it to its neighbour.
  - If the distance is lower than the `tentative distance` of the neighbour node, then replace the tentative distance of the neighbour node.
- Once all the neighbour nodes are visited. Mark the current node as visited, exclude it from the unvisited nodes.
- If the destiantion node has been visited, the algorithm end here.
- Otherwise select the unvisited node with the smallest `tentative distance`. It will be the current node.
