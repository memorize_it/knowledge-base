---
id: 20210110122750
tags: []
references: []
---
# Nextjs with react-helmet

- Check the example on [GitHub](https://github.com/vercel/next.js/tree/master/examples/with-react-helmet)

## Add react-helmet

```bash
yarn add react-helmet
yarn add -D @types/react-helmet
```

## On pages/_app.tsx

```typescript
import React from 'react';
import { AppProps } from 'next/dist/next-server/lib/router/router';
import { Helmet } from 'react-helmet';

const APP_NAME = 'next-pwa example';
const APP_DESCRIPTION = 'My custom PWA';

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <>
      <Helmet
        ...
      />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
```

## On pages/_document.tsx

```typescript
import React, { HTMLAttributes } from 'react';
import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentInitialProps,
  DocumentContext,
} from 'next/document';
import { Helmet, HelmetData } from 'react-helmet';

type MyDocumentProps = DocumentInitialProps & {
  helmet: HelmetData;
};

export default class extends Document<MyDocumentProps> {
  static async getInitialProps(ctx: DocumentContext): Promise<MyDocumentProps> {
    const documentProps = await super.getInitialProps(ctx);
    // see https://github.com/nfl/react-helmet#server-usage for more information
    // 'head' was occupied by 'renderPage().head', we cannot use it
    return { ...documentProps, helmet: Helmet.renderStatic() };
  }

  // should render on <html>
  get helmetHtmlAttrComponents(): HTMLAttributes<HTMLHtmlElement> {
    return this.props.helmet.htmlAttributes.toComponent();
  }

  // should render on <body>
  get helmetBodyAttrComponents(): HTMLAttributes<HTMLBodyElement> {
    return this.props.helmet.bodyAttributes.toComponent();
  }

  // should render on <head>
  get helmetHeadComponents(): unknown[] {
    return Object.keys(this.props.helmet)
      .filter((el) => el !== 'htmlAttributes' && el !== 'bodyAttributes')
      .map((el) => this.props.helmet[el].toComponent());
  }

  render(): JSX.Element {
    return (
      <Html {...this.helmetHtmlAttrComponents}>
        <Head>{this.helmetHeadComponents}</Head>
        <body {...this.helmetBodyAttrComponents}>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

```
