---
id: 20210412150009
tags: []
references: []
---
# Observables vs Promises

- Promise :
  - As soon as it is made made, the execution takes place.
  - handle a single event
- Observables :
  - Lazy. Nothing happens until a subscription is made.
  - Stream that allows passing of more than one event. A callback is made for each event in an observable.
