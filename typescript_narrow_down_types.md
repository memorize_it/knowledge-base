---
id: 20221004115911
tags: []
references:
 - 20210608121324
---

# Typescript: Narrow down types

How to use an assertion function in typescript.

```typescript
function assertNonNullish<TValue>(
  value: TValue,
  message: string
): asserts value is NonNullable<TValue> {
  if (value === null || value === undefined) {
    throw Error(message);
  }
}
```

Source : [Marius Schulz's blog](https://web.archive.org/web/20221004100039/https://mariusschulz.com/blog/assertion-functions-in-typescript)
