---
id: 20210716173333
tags: []
references:
 - 20210325170539
---

# Java Security

- Use `java.net.URL` rather than `java.net.URI` for ip address resolving.
  - `java.net.URI` returns null if we provide an ip with a [other format](20210716173000) than the usual (127.0.0.1).
