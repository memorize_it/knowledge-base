---
id: 20220120114633
tags: []
references: []
---

# AtomicReference vs volatile

[`AtomicReference`](20220120114435) is almost the same as using [`volatile`](20220120113043) in case of accessing the value just with `get` and `set` methods.

[`AtomicReference`](20220120114435) allows to ensure that the reference is only updated once for all the threads if using [`compareAndSet`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicReference.html#compareAndSet(V,V)) method.
