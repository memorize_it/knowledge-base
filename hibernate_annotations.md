---
id: 20210329101547
tags: []
references: []
---
# Hibernate annotations

- On class level : @Entity (we can specify a table name)
- On field level:
  - **Must** : @Id on primary key
  - @Column : If the field name and column name doesn't match
  - @Transient : If field must not be mapped
