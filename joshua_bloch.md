---
id: 20211216120744
tags: []
references:
 - 20211216120449
---

# Joshua Bloch

- [Twitter](https://twitter.com/joshbloch)
- [GitHub](https://github.com/jbloch)
- [Email](josh@bloch.us)
