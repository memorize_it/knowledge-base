---
id: 20210108163102
tags: []
references: []
---
# React PWA setup : With Next.js

Check this articles :

- [PWA with Next.js (create-next-app) in 2020](https://itnext.io/pwa-with-next-js-create-next-app-in-2020-%EF%B8%8F-9ee0e1a6313d).
- [How to create a PWA with react](https://www.codica.com/blog/how-to-create-pwa-with-react/)

Generate assets and move them to the `public` foder.

- manifesto on [app-manifest](https://app-manifest.firebaseapp.com/)
- icons on [PWA Image generator](https://www.pwabuilder.com/imageGenerator).
- favicons on [Favicon generator](https://www.favicon-generator.org/)
