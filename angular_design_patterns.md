---
id: 20210413154453
tags:
    - design_pattern
    - angular
references: []
---
# Angular design patterns

## Bridge

Watch it on [YouTube](https://www.youtube.com/watch?v=2rQOu9TmuxE)

Allows to trigger child component events from parent.

- On a parent component, use `ng-content` to display the content of the tag.
- The different possible child components will implement a given interface
- we get the reference with an Injection Token
  - create the token : `export const TOKEN = new InjectionToken<ChildInterface>('token')`
  - On the child component

  ```js
  @Component({
      ...
      providers: [
          {
              provide: TOKEN,
              useExisting: ComponentClass
          }
      ]
  })
  ```

  - inside the parent component we get the reference to the child using `@ContentChild<TOKEN>` and assign to a private property
- We will listen to something (click) on the parent component and call a method
- Inside the method we can trigger the interface method, and the child implementation of the method will be called
