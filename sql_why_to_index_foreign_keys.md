---
id: 20220601101135
tags: []
references:
 - 20210608121348
---

# SQL Why to index foreign keys

When creating a foreign key, we **should index the column referenced by the foreign key**. This will improve db performance for example when deleting a row in the table targeted by the foreign key, avoiding a `Seq scan`.

We may not need to index if rows can not be deleted in the table targeted by the FK or if the table is small.

In this example we should index column `plant_name` in table `plant`:

```mermaid
erDiagram
    plant {
        integer plant_id PK "Plant id"
        integer plant_name "Regular column" "Plant name"
        text name
    }

    crop {
        integer id
        integer plant_name FK "Cultivated plant name"
        integer yeld
    }
```
