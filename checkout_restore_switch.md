---
id: 20210805093957
tags: []
references:
  - 20210608121259
---

# Checkout = Restore + Switch

You can chek [this post](https://www.banterly.net/2021/07/31/new-in-git-switch-and-restore/) for more information.

## Git checkout

Git checkout can be used for two things :

- Change branch : `git checkout develop`
- Restore a file to HEAD : `git checkout file.txt`

Two new commands where added to git on [2.23](https://github.com/git/git/blob/master/Documentation/RelNotes/2.23.0.txt) to split this two functionalities on different commands, `switch` and `restore`.

## Git switch

- Change branch : `git switch develop`

## Git restore

- Restore a file to HEAD : `git restore file.txt`
