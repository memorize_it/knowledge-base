---
id: 20211207133524
tags: []
references: []
---

# Advent of code

## Lessons learned

- 2021-7 :
  - [The median](20211207134225) minimizes the sum of [absolute deviations](20211207134227)
  - The sum of all the integer numbers up to n can be calculated using [the triangular number](20211207135629).
- 2021-15 :
  - [Dijkstra's algorithm](20211216141209) resolves the shortest path problem in a weighted graph.
