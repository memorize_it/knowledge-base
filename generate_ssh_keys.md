---
id: 20210108155327
tags: []
references: []
---
# Generate ssh keys

```bash
ssh-keygen -t ed25519 -C 'Comment'
xclip -sel clip < ~/.ssh/id_ed25519.pub
```
