---
id: 20211126155946
tags: []
references:
 - 20211125093626
---

# Kafka Disk performance

- Flushing and reading full segments of disk is why a **better throughput is achieved with smaller message sizes**

## Disk representation

- Kafka writes active messages in a segment
- When segment is full (configurable) data is flushed to diks, and new segment is created
- When data is flushed to disk
  - log file is created, with the actual data, the file is named after the first offset on the segment
  - index file is created, with a reference to all offsets in the log file
