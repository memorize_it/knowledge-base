---
id: 20210413145323
tags: []
references: []
---
# Redux talks

- [Redux Tutorial: Learn Redux from Scratch - Mosh Hamedani](20210413101543)
- [Advanced Redux: Design Patterns and Practices - Nir Kaufman](20210413140109)
- [Practical Advanced Redux - Boris Dinkevich](20210413145518)
