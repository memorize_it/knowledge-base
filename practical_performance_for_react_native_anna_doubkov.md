---
id: 20210407165103
tags:
    - talk
    - react
    - youtube
references: []
---
# Practical Performance for React Native - Anna Doubková

Watch it in [YouTube](https://www.youtube.com/watch?v=jTdi9oTM22c).

Tips :

- React performance monitor
  - UI and JS threads should run at 60 fps
- Check performance from ReactNative or Chrome DevTools
- ShouldComponentUpdate is not always the solution
- Avoid using anonymous functions on component inputs
  - New reference is created on every render
- React-Redux will make that every time the state updates the function will be called
  - Avoid using mergeProps
  - Try to use mapDispatch as object
- Prefer inline requires of images and if possible of native modules
- Routing oun react-native : prefer react-native-navigation or react-navigation
