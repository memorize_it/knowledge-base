---
id: 20210325170726
tags: []
references: []
---
# Hibernate concepts

- Session: Short living object. Query objects, manages JDBC connections. Holds a first-level cache.
- Sessions are no thread safe.
- Session factory: one per database, manages session creation.
- Criteria API: build queries through an API.
- Query API: uses HQL (hibernate query language), similar to SQL
- Open session vs getCurrentSession
  - Open creates new object, slower in single threaded, flush and close session
  - Get is managed by hibernate, faster
- Get vs Load
  - Get if no sure of object existence, null if not existing
  - Load if sure, it will throw if not exisitng
- First level cache : session
- Second level cache : Session factory, not enabled by default
- Collection types : Bag, Set, List, Map, Array
