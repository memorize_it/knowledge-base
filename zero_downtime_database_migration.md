---
id: 20220505145245
tags: []
references:
  - 20210608121348
---

# Zero downtime database migration

From the [Montée de version sans interruption](https://www.youtube.com/watch?v=pIkA-aPtkNs) talk by Nelson Dionisi at Devox 2022.

Principle :

- Your N database version has to be compatible with the N-1 application.

## Mechansims

### Drop column

| Version | Database                     | App                                    |
| ------- | ---------------------------- | -------------------------------------- |
| V1      | No modification              | Read + Write                           |
| V1      | Supress NOT NULL constraints | Write (in case of rollback) + Not Read |
| V3      | -                            | Not Read + Not Write                   |
| V4      | Drop the column              | -                                      |

### Add column

| Version | Database                        | App                                                                  |
| ------- | ------------------------------- | -------------------------------------------------------------------- |
| V1      | No column                       | Nothing to do with the column                                        |
| V1      | Create column but it's nullable | Write (in case of rollback) + Not Read (Nulls will be in the column) |
| V3      | Backfill the column             | Read + Write                                                         |
| V3      | Add NOT NULL constraint         | -                                                                    |

## Respect the constaints

- Do not `INSERT/UPDATE` while we are not instended to Write
- Do not `SELECT` while we are not instended to Read
  - We can not use `SELECT *`
