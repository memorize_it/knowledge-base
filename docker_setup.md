---
id: 20210108130555
tags: []
references: []
---
# Docker setup

Check [official  documentation](https://docs.docker.com/engine/install/ubuntu/).

[Post install](https://docs.docker.com/engine/install/linux-postinstall/) documentation.

```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# Add user to docker to avoid need of sudo
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker 

docker run hello-world
```
