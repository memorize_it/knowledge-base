---
id: 20220523171640
tags: []
references:
 - 20220523171457
---

# Transactions in spring batch jobs

[Transactions](20210402083146) should be opened at writer level, because they should be retryable on rollback, and some readers may not support the retry (ex: a file writier using a cursor)
