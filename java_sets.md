---
id: 20210401092721
tags: []
references: []
---
# Java sets

- All the objects **must implement equals and hashCode**, necessary to asses if the element does already exist in the collection.

## Interfaces

- SortedSet
  - First, last, headSet, tailSet...
- NavigableSet:
  - ceiling, descendingIterator, ascendingIterator...

## Implementations

- HashSet : not synchoronizd
  - Accepts null
- TreeSet : not synchoronizd
  - ordered
    - based on the Comparable interface
    - based on a comparator, supplied on creation
  - Doesn't accept null
- CopyOnWriteArraySet : thread safe
- EnumSet
- LinkedHashSet
- ConcurrentSkipListSet
  - For many concurrent access
