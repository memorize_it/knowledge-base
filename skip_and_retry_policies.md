---
id: 20220523171641
tags: []
references:
 - 20220523171457
---

# Skip and retry policies

Retry policy : will retry the step again, with the **same input**
Skip policy : will **retry the step for every element**, separately, and ignore the element(s) with an issue
