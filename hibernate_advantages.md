---
id: 20210325170141
tags: []
references: []
---
# Hibernate advantages

- [Lazy Loading](20210401150034)
- Caching
- No JDBC code => need to write less code
- It provides high level object oriented API
