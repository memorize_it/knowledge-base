---
id: 20221121164409
tags: []
references: []
---

# XState first impressions

These are my first impressions on [XState](20221121164203).

- The `initialState` should be extracted into an external file, with the explanations on why these defaults. When I want to update an intermediate state, I do not necessarilmy care of the initial state and this would allow me to read less code when doing so.
- If the initialState is declared as a string, then the keys of the states ob ject, should be the same constants
- All the actions and state names should be listed as a constants, so that we can access them easily and see all the possible states.
- The different blocks of the states where most of the code is should be extracted, in order to improve overall readibility, this includes :

  `entry, src`

  These values could be declared in the `services` section, even if they are not reused

- When we do have binary conditions (`if... else...`) we could avoid declaring the condition twice, xState can manage this for us using `choose(...)` which will apply the first truthy condition
- When we do have conditions, perfer using string delcarations of guards, rather than function implementations

  ```js
  // Avoid:
  cond: context =>
    context.dataFetched && context.dataMapped
  // Prefer:
  cond: ['dataFetched', 'dataMapped'];
  ...
  guards: {
    dataFetched: context => context.dataFetched ,
    dataMapped: context => context.dataMapped
  }
  ```

- When triggering an action is just a matter of calling send :

  ```js
  // Avoid:
  actions: send({
    type: "NEW_OPEN_LIST",
  });
  // Prefer:
  actions: ["NEW_OPEN_LIST"];
  ```

- When we do have onDone actions that modify the context, we should extract and name them on how they are modifying the context.
- In the asnyncrhonous service calls, we should not have logic in the then parts, instead we could amnage it in the `onDone`
- We can send delayed events, this can be used for eaxmple for polling if the received response from an api was not sufficient for a state transition and we need to perform another call

  ```js
  {
    actions: send('AI_AUTOMAP_RETRY', {
      delay: 2000,
    }),
  }
  ```
