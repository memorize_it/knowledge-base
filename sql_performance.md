---
id: 20210325172540
tags: []
references: []
---
# SQL Performance

- Views over long queries.
- Constraints over triggers. For consistency and performance.
- Union All is faster than union, it does not look for duplicates.
- Avoid DISTICST and HAVING.
- Create indexes.

## Bottlenecks

- CPU problems
- Memory
- External storage use

## Explain plan

Explains how to manage the table, so that access is optimized.
