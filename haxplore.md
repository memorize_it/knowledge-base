---
id: 20210930142745
tags: []
references:
 - 20210930142716
---

# Haxplore

[Haxplore](https://haxplore.pabue.co/) is a hacker news client with keyboard navigation.

## Screen-shots

![Home page](./images/haxplore_home.png)
![Details page](./images/haxplore_detail.png)
![Home help overlay](./images/haxplore_help.png)
![Home page - mobile view](./images/haxplore_mobile.png)
![Detail page - mobile view](./images/haxplore_mobile_detail.png)

## What do I like

- **Minimalist**
- Text focused
- Keyboard first
- Keyboard shortcuts displayed alongside information (very intuitive)
- Mobile friendly
