---
id: 20220930134811
tags: []
references:
 - 20210930142716
---

# MusiCo

[MusiCo](https://www.musi-co.com) is an AI generated music site.

## Screen-shots

![Loading](./images/musico_loading.png)

## What do I like

- The color palette : How the brown and orange shades are integrated

## What do I do not like

- The interface looks really like spotify
