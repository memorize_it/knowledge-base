---
id: 20221121164203
tags: []
references:
 - 20210608121324
---

# XState

[Official Documentation](https://xstate.js.org/docs/)

JavaScript and TypeScript finite state machines and statecharts for the modern web.
