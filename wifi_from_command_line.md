---
id: 20200913130033
tags: []
references: []
---
# Wifi from command line

1. Scan for interface : `sudo ifconfig`
1. Check for networks : `sudo iwlist wlan0 scan`
1. Open wpa_supplicant.config : `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`
1. Add :

```plaintext
network : {
    ssid="NetworkName"
    psk="NetworkPsw"
    scan_ssid=1
}
```

1. Restart wpa_supplicant : `sudo wpa_supplicant -B -i wlan0`
