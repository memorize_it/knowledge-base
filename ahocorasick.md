---
id: 20220117175609
tags: []
references:
 - 20210108162425
---

# Aho-Corasick

String #searching #algorithm

Search the input text for words included in a dictionary (known set of string).

THe algorithm will build a finite-state machine, like a trie.

## Usage tips

The process of generating the trie may be slow. If the same dicitonary is to be used multiple times, **the trie should only be generated once** per dictionary and then used on the different inputs.
