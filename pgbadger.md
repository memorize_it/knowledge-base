---
id: 20210616145922
tags: []
references: []
---

# Pgbadger

## Install

Download latest release from [GitHub](https://github.com/darold/pgbadger/releases).

```bash
tar xzf pgbadger-11.x.tar.gz
cd pgbadger-11.x/
perl Makefile.PL
make && sudo make install
```

## Generate log

Given

```bash
export POSTGRESQL_LOG_DIR="/var/log/postgresql"
```

Go to the log directory and **generate the html report**, then open it with a web browser

```bash
cd $POSTGRESQL_LOG_DIR
pgbadger postgresql.log 
firefox out.html
```

Filter logs by time

```bash
pgbadger postgresql.log --begin '2022-05-18 10:30:00' --end '2022-05-18 12:00:00'
firefox out.html
```
