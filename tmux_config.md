---
id: 20210622092744
tags: []
references: []
---

# Tmux config

```tmux
# Reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf

# Switch panes using Alt-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Fix issue with incorrect Vim colors
set -g default-terminal "xterm-256color"

# Remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# Start window numbering at
set -g base-index 1

# Enable mouse control (clickable windows, panes, resizable panes)
set -g mouse on

# Disable escape time
set -sg escape-time 0

# ----------------------
# Status Bar
# -----------------------
set-option -g status on              # turn the status bar on
set -g status-interval 1             # set update frequencey (default 15 seconds)
set -g status-justify left           # center window list for clarity

# set color for status bar
set -g status-style bg=default

# set window list colors - red for active and cyan for inactive
setw -g window-status-style fg=brightblue,bg=default
setw -g window-status-current-style fg=green,bg=default

set -g status-left ""
set -g status-right "#(cd #{pane_current_path}; git rev-parse --abbrev-ref HEAD)"
set -g status-right-style fg=green,bg=default
set -g status-right-length 100

set -g default-terminal "screen-256color"

# start a non-login shell to improve performance
set -g default-command "${SHELL}"

# Remove the annoying asterisk.
setw -g window-status-current-format '#{?window_zoomed_flag,#[fg=red],}#I #W#{?window_zoomed_flag,#[fg=red],}'
setw -g window-status-format '#I #W'

# Set scrollback history to 10000 (10k)
set -g history-limit 10000

# Change split pane shortcuts

bind / split-window -h # Split panes horizontal
bind - split-window -v # Split panes vertically
unbind '"'
unbind %

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @resurrect-capture-pane-contents 'on'
set -g @continuum-boot-options 'alacritty,fullscreen'

set -g terminal-overrides ',xterm-256color:Tc'
set -g default-terminal "tmux-256color"
set -as terminal-overrides ',xterm*:sitm=\E[3m'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
```
