---
id: 20220512142613
tags: []
references:
 - 20210608121348
---

# The n+1 problem

The problem consists in fetching a list of parent element (1) and then fetching the children separatelly for every parent (N). This ends with `N+1` requests being executed.

This can be solved in many ways :

- by consolidating the request for all the children in a single request. 1 Request for the parents, 1 Request for the children. And then mapping in memory.
- by performing a single request that will return the joined results. 1 single request. And them extract the entities from the results.
  - using hibernate this can be achieved using a [fetch join](20220512153731).
