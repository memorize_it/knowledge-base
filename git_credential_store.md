---
id: 20210317115025
tags: []
references: []
---
# Git credential store

```bash
git config credential.helper store
```
