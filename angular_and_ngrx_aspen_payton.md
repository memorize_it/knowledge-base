---
id: 20210412170339
tags:
    - talk
    - tutorial
    - angular
    - ngrx
    - youtube
references: []
---
# Crash Course : Angular and ngRx Aspen Payton

Watch it on [YouTube](https://www.youtube.com/watch?v=272KDxSIQBw).

## Setup

Let's imagine a Banana :banana: as our current **state**.

```js
export interface State {
    isPeeled: boolean;
    bytesRemaining: number;
    color: string
}
```

If we want to update our banana, we need to track **actions**.
Actions will have a type and a payload with the related information.

For example, to get a new banana :

```js
import { Action } from "@ngrx/store";

export const GET_NEW_BANANA = "Get new banana";

export class GetNewBanana implements Action {
  readonly type: string = GET_NEW_BANANA;

  constructor(public payload: any){
    console.log('ACTION' + GET_NEW_BANANA);
  }
}

export type BananaAction = GetNewBanana;
```

Now we need something to update our banana's state : a **reducer**.

```js
import { GET_NEW_BANANA } from "./banana.actions";
import * as programActions from "./banana.actions";

export function reducer(state: any, action: programActions.BananaAction) {
  switch (action.type) {
    case GET_NEW_BANANA: {
      console.log("REDUCER " + GET_NEW_BANANA);
      return { isPeeled: false, bitesRemaining: 9, color: "yellow" };
    }
    default: {
      return {
        ...state
      };
    }
  }
}
```

If we have multiple state objects (apple, watermelons...), we can track the state on an application level, where we point to the different **slices of state**.

We also need to keep track of our Bananas state, we will use a **store** on the app level.

```js
import {ActionReducerMap} from '@ngrx/store';
import * as bananaStore from './banana/state';

export interface AppState {
  banana: bananaStore.State;
}

export const initialState: AppState = {
  banana: bananaStore.initialState
}

export const reducers : ActionReducerMap<AppState> = {
  banana: bananaStore.reducer
}

export const getMyBanana = (s: AppState)=> s.banana;
```

We also need to tell App we are using ngRx. On the app.module.ts :

```js
StoreModule.forRoot(reducers, { initialState }),
StoreDevtoolsModule.instrument({ maxAge: 25 })
```

We can then use the banana in our component.

```js

@Component({
...
})
export class BananaComponent implements OnInit {
  banana$: Observable<any>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.newBanana();
    this.banana$ = this.store.pipe(select(getMyBanana))
  }

  newBanana() {
    this.store.dispatch(new GetNewBanana(null));
  }
}
```

## Add new action

In order to eat the banana, we need to peel it :banana: .

So we will add a new action.

```js
export const PEEL_BANANA = "Peel banana";
...
export class PeelBanana implements Action {
  readonly type: string = PEEL_BANANA;

  constructor(public payload: any){
    console.log('ACTION ' + PEEL_BANANA);
  }
}
...
export type BananaAction = GetNewBanana | PeelBanana;
```

Then we need to update our reducer.

```js
export function reducer(state: any, action: BananaAction): any {
  switch (action.type) {
    case GET_NEW_BANANA:
      ...
    case PEEL_BANANA: {
      console.log("REDUCER " + PEEL_BANANA);
      return { ...state, isPeeled: true };
    }
    default:
      ...
  }
}
```

Finally we bind this action to our component.

```js
@Component({...})
export class BananaComponent implements OnInit {
  ...
  peelBanana() {
    this.store.dispatch(new PeelBanana(null));
  }
  ...
}
```

## Action with payload

We act as with the previous actions, but we will pass the action payload to the action.

```js
@Component({...})
export class BananaComponent implements OnInit {
  ...
  eatBanana() {
    this.store.dispatch(new EatBanana(3));
  }
  ...
}
```

## Side effects

When we need a side **effect** triggered after an action, we need to create **two actions**. One to trigger the effect, and another to apply the effect after we have the response.

```js
...
export const INITIATE_TIME_HOP = "Initiate time hop";
export const TIME_HOP_COMPLETE = "Time hop complete";
...
export class InitiateTimeHop implements Action {
  readonly type: string = INITIATE_TIME_HOP;

  constructor(public payload: number) {
    console.log("ACTION " + INITIATE_TIME_HOP);
  }
}

export class TimeHopComplete implements Action {
  readonly type: string = TIME_HOP_COMPLETE;

  constructor(public payload: number) {
    console.log("ACTION " + TIME_HOP_COMPLETE);
  }
}

export type BananaAction =
  | GetNewBanana
  | PeelBanana
  | EatBanana
  | InitiateTimeHop
  | TimeHopComplete;
```

We update the reducer. To manage the new actions.

```js
import {
  BananaAction,
  EAT_BANANA,
  INITIATE_TIME_HOP,
  PEEL_BANANA,
  TIME_HOP_COMPLETE
} from "./banana.actions";
import { GET_NEW_BANANA } from "./banana.actions";

export function reducer(state: any, action: BananaAction): any {
  switch (action.type) {
    ...
    case INITIATE_TIME_HOP: {
      console.log("REDUCER " + INITIATE_TIME_HOP);
      return {
        ...state
      };
    }
    case TIME_HOP_COMPLETE: {
      console.log("REDUCER " + TIME_HOP_COMPLETE);
      return {
        ...state,
        color: action.payload
      };
    }
    ...
  }
}
```

We need a service that will actually rot our banana after a while. We will create `rot.service.ts` :

```js
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class RotService {
  rotBanana(): Observable<any> {
    console.log("ROT BANANA");
    const milliseconds = 10000;
    return new Observable(observer => {
      setTimeout(() => {
        console.log("Done roting");
        observer.next("brown");
        observer.complete();
      }, milliseconds);
    });
  }
}
```

In order to trigger the action chain, we will use an **effect**. We will create a `banana.effects.ts` :

```js
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs/operators";
import { RotService } from "../../rot.service";
import {
  InitiateTimeHop,
  INITIATE_TIME_HOP,
  TimeHopComplete
} from "./banana.actions";

@Injectable()
export class BananaEffects {
  constructor(private actions$: Actions, private rot: RotService) {}

  @Effect()
  public InitiateTimeHop$ = this.actions$.pipe(
    ofType(INITIATE_TIME_HOP),
    switchMap((action: InitiateTimeHop) =>
      this.rot.rotBanana().pipe(map(color => new TimeHopComplete(color)))
    )
  );
}
```

To make effect works, we need to update :

`index.ts`

```js
...
export { BananaEffects } from "./banana.effects";
...
```

`app.state.ts`

```js
...
export const effects: Array<any> = [bananaStore.BananaEffects];
...
```

`app.module.ts`

```js
...
EffectsModule.forRoot(effects),
...
```
