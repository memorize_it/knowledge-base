---
id: 20210622095945
tags: []
references:
 - 20210527094612
---

# TMux

[Github](https://github.com/tmux/tmux) repository.

- [Customizing the conf](https://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/)
- [Example dotfiles](https://github.com/semanser/dotfiles)
- [Tmux config](20210622092744)
- [Tmux shortcuts](20210622092745)
