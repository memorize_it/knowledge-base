---
id: 20210412153110
tags: []
references: []
---
# Redux

A library that helps manage the state of an application. Based on Flux architecture.

## Pros

- Centralized application state.
- Decouples the application from the presentation framework.
- Allow to easily track data evolution over time: What, When, Why, Where...
- Simplifies the testing. Uses functional programming.
- Great tooling
  - [Redux DevTools](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/)
  - Allow time-travel debugging.

## Cons

- Complexity
- Need to know functional programming

## When to use Redux ?

- Many copies of same data without parent/children relation
- Need to preserve page state
- Data can be updated by multiple users
- Data can be updated by multiple actors

## When not to use Redux ?

- No much time, one-shot project that won't evolve
- Small app
- Static data

## How it works

### Store

JS object, the current **state** of the application.
Shared through all the application.

Has the responsability to :

- receive the action
- call the reducer
- notify state changes

### Actions

JS object, represents an event.
Something that **alters the state**.
Must have a type property. Convetion says to use strings, as they are human-friendly. We can use other types.
May have other properties (payload) containing related data.
May have other properties (meta) containing data for middlewares.

### Reducers

Pure functions, creates the **new state after a given action**.
Two parameters :

- the current state
- an action
  
Has the responsability to :

- reducers **does not modify the sate**. It will just return the new state.

One application may have many reducers, one responsible of updating an slice of the store.

### Middlewares

Functions that will trigger bassed on actions. They create a pipeline that will apply to actions.
We put async code or side-effects here.

## Using redux

From [Redux Tutorial - Learn Redux from Scratch](20210413101543)

### Design the store

Decide the data structure of the application. Every top-level property is an slice.

### Define the actions

What the user can do. The different things that may update our state.
Action payloads must only have minimal required information.
We can setup action creators, so that we can easily dispatch new actions.
This will avoid having to setup the type on every dispatch, making our code easier to refactor.

### Create the reducer

The reducers will create the new state.
The reducer will enrich the action, it is responsible of adding the BL.
If the action is unknown we need to return the previous state, to avoid errors.
We can add a default value to previous state. The initial state.

### Setup the store

We will create an store, and pass the reducer.

```js
import {createStore} from 'redux';
import reducer from './reducer';
const store = createStore(reducer);
export default store;
```
