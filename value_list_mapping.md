---
id: 20220106102750
tags: []
references:
 - 20210723143307
---

# Value list mapping

When we want to map value lists, the system should automatically select the next value to map.

We should make easier for the people mapping the values to just make the choice.

| Country | City    |
|---------|---------|
| France  | Paris   |
| China   | Beijing |
| Nigeria | Lagos   |

In the example above :

- we would select `France` when landing in the page
- when the uers click on `Paris`, the system would automatically select `China`

In the best case scenario, this allows the user to perform a single click for value-pair.
