---
id: 20211125111933
tags: []
references:
 - 20211125093626
---

# Kafka Guarantees

- at most once : you will only consome a message once, but you may not consume it
- at least once : receipt guaranteed, can be processed n times
- exactly once : message will be consommed only once
