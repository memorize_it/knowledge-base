---
id: 20210325175709
tags: []
references: []
---
# Hibernate entity states

- Transient
- Persistent
- Detached
- Removed

## Transient state

- Once we create an instance of POJO class, then the object entered in the transient state.
- Not associated with the Session. So, the transient state is not related to any database.
- Modifications in the data don't affect any changes in the database.
- The transient objects exist in the heap memory. They are independent of Hibernate.

## Persistent state

- Associated with the Session, it entered in the persistent state.
- Hence, we can say that an object is in the persistence state when we save or persist it.
- Here, each object represents the row of the database table.
- So, modifications in the data make changes in the database.

```java
session.save(e);  
session.persist(e);  
session.update(e);  
session.saveOrUpdate(e);  
session.lock(e);  
session.merge(e);  
```

## Detached State

- Once we either close the session or clear its cache, then the object entered into the detached state.
- No more associated with the Session, modifications in the data don't affect any changes in the database.
- Still has a representation in the database.
- If we want to persist the changes made to a detached object, it is required to reattach the application to a valid Hibernate session.
- To associate the detached object with the new hibernate session, use any of these methods - load(), merge(), refresh(), update() or save() on a new session with the reference of the detached object.

## Removed state

Schedules a deletion operation, which will apply on session flush.
