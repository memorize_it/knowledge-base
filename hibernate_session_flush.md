---
id: 20210401150908
tags: []
references: []
---
# Hibernate session flush

See [Vlad Mihalcea's blog](https://vladmihalcea.com/a-beginners-guide-to-jpahibernate-flush-strategies/).

The **flushed** changes are visible only for the **current database transaction**.
Until the current transaction is **committed**, no change is visible by **other concurrent transactions**.
See [Flush vs Commit](20210402085142).

The persistence context, also known as the **first level cache**, acts as a buffer between the current entity state transitions and the database.

Hibernate flush modes :

- Auto : The Session is **sometimes** flushed **before** query execution.
  - When using **JPA**, the AUTO flush mode causes **all queries** (JPQ, Criteria API, and native SQL) to trigger a flush **prior to the query execution**. [Source](https://vladmihalcea.com/how-does-the-auto-flush-work-in-jpa-and-hibernate/).
- Commit : The Session is **only** flushed prior to a **transaction commit**.
- Always : The Session is **always** flushed **before query execution**.
- Manual : The Session can only be manually flushed.
- Never : Deprecated. Use MANUAL instead. This was the original name given to manual flushing, but it was misleading users into thinking that the Session won’t ever be flushed.
