---
id: 20210316112450
tags: []
references: []
---

# Android emulator point to localhost server

In order to point an app running inside an #android #emulator to a server running in the localhost, the app should be configured to point: `http://10.0.2.2:port` with the correct port.
