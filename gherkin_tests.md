---
id: 20210117101053
tags: []
references: []
---
# Gherkin tests

- Given : somethig that took place in the past.
- When : what is to be tested.
- Then : what you want to happen on the system.
