---
id: 20210407154536
tags: []
references: []
---
# React talks

- [Refactoring React: Which component pattern can improve your codebase? - Siddharth Kshetrapal](20210407154750)
- [Practical Performance for React (Native): How common patterns slow down your app - Anna Doubková](20210407165103)
