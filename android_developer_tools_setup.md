---
id: 20210108113427
tags: []
references: []
---
# Android developer tools setup

Make sure [Java is installed](20210108115151).

Check the [Android developers website](https://developer.android.com/studio/#downloads) and download the command line tools.

```bash
mkdir ~/DevTools
mkdir ~/DevTools/Android
mkdir ~/DevTools/cmdline-tools
unzip /tmp/commandlinetools-linux-6858069_latest.zip -d ~/DevTools/Android/cmdline-tools
mv ~/DevTools/Android/cmdline-tools/cmdline-tools ~/DevTools/Android/cmdline-tools/tools
echo 'export ANDROID_HOME="$HOME/DevTools/Android"\nexport ANDROID_SDK_ROOT="$ANDROID_HOME"\nexport PATH="$ANDROID_HOME/cmdline-tools/tools/bin:$ANDROID_HOME/platform-tools:$ANDROID_HOME/emulator:$PATH"' >> ~/.zshrc
source ~/.zshrc
```

## Create android virtual device

```bash
sdkmanager "platform-tools" "platforms;android-30" "build-tools;30.0.3"
sdkmanager "system-images;android-30;google_apis;x86_64"
sdkmanager --licenses
sdkmanager --update
avdmanager create avd -f -n "Nexus_4_API_30_x86_64" -k "system-images;android-30;google_apis;x86_64"
```
