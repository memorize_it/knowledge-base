---
id: 20220822170200
tags: []
references: []
---

# Deleting large amount of items and notifying other systems

## What do we want

- Delete a large amount of rows (Y) in the database (or all the rows)
- Send a message to notify another system of the deleted items, by chunks of N size

## The system

- The **repository** : access the database
  - It can delete elements
  - It must return the ids of the deleted elements
  - It can return in a lazy way all the elements meeting a condition (by N sized chunks)
- The **indexer** : notifies the other system with the deleted items
- The **service** : orchestrates interactions between repository and indexer
  - triggers the deletion of a given amount of elements
  - calls the indexer

## Options

### Service is main actor

Service handles the whole thing in a single transaction

It can be done with a chunked delete

- It requests the repository n-sized chunks of elements
- For every chunk :
  - Delete the elemetns
  - Notify the other system about these items if the transaction succeeds

With a complete delete

- Request all the elements meeting the condition in a chunked way to the repository
- For every chunk, notify the other system if the transaction succeeds
- Delete all the elements meeting the condition

### Service is an executor

Service handles this in multiple transactions

- Service exposes the required tools
  - A method to get the elements meeting a condition, with a limit
  - A method to delete and index elements for a given chunk (receives the ids)
