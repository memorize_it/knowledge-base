---
id: 20210414121055
tags: []
references: []
---
# Mobile app development strategies

- Native: use native SDK
  - Android and apple SDK
- Cross-Platform : common code to many platforms and then transpile code to native code
  - Ract native, xamarin and flutter
- Hybride : common code to many platforms that is executed inside a webview
  - Cordova + Ionic and capacitor
- PWA : responsive + serviceWorkers, with access to some native interfaces
  - Angular or React
