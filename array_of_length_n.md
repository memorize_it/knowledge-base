---
id: 20221013145033
tags: []
references:
  - 20210608121324
---

# Array of length n

To get an actual array with n elements in javascript we can do :

```javascript
// In two steps :
const nElementArray = Array.from({ length: n });
const filledArray = nElementArray.map((n, i) => i);

// Or :
const filledArray = Array.from({ length: n }).map((_, i) => i);
```
