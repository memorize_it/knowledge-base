---
id: 20220120141512
tags: []
references:
 - 20220120141102
---

# Bloat score

The `bloat score` is the ratio between dead records and active records. Higher the score, less efective the query planner will be.

`dead/active`
