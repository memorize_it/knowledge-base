---
id: 20211125104038
tags: []
references:
 - 20211125093626
---

# Kafka Topics

- Represent a business logic concept
- Like a queue in traditional messaging systems
- Every message will be in a topic (1-n relation)
- Can be configured
  - Message lifetime
  - Max message size
  - Compression type
- [Topic performance and paritioning](20211126154846)

## Partitioning

- 1 topic 1+ partitions
- 1 partition is assigned to 1 [broker](20211125105813)
- We need to avoid overload, partitions should allow to balance load
- Assign messages to paritions based on the key
  - We may compose key (ex : os + device id)
  - if no key [round robing](20211125104809)
  - we can provide custom algorithms
- The producer may choose where to write

## Replicas

- 1 partition 1+ replicas
- How many copies of every partition we will have
- Allows high availability
- Leaders & followers
  - Followers in another brokers
- The producers tells when to consider that the message is written
  - Ex : only after n replicas got the message

## Compaction

- For every key keep at least 1 value
- Can ignore older values for the same key
- Avoidkeeping many update records, keep latest updates

## Compression

- gzip : fast compression lower decompression
- snappy : balanced
- lz4 : slow compression fast decompression

## Cli

```bash
# kafka-topics --zookeeper <ZOOKEEPER_ADDRESS> --topic <TOPIC_NAME> --create --partitions <PARTITION_NUMBER> --replication-factor <REPLICATION>
kafka-topics
kafka-topics --zookeeper 127.0.0.1:2181 --list
kafka-topics --zookeeper 127.0.0.1:2181 --topic my_topic--create --partitions 3 --replication-factor 1
kafka-topics --zookeeper 127.0.0.1:2181 --topic my_topic --describe
```
