---
id: 20210330101937
tags: []
references: []
---
# Inversion of control

[Baeldung : IoC in Spring](https://www.baeldung.com/inversion-control-and-dependency-injection-in-spring)

Inversion of Control is a principle in software engineering which transfers the control of objects or portions of a program to a container or framework.
