---
id: 20210414104530
tags:
    - rxjs
references: []
---
# RxJs

Javascript implementation of reactive programming.

[Official documentation](https://rxjs.dev/)
[GitHub](https://github.com/ReactiveX/rxjs)

## Observables

Create the observable : `Observable.create(producerFunction)`.
We **publish** content using : `observer.next(content)` inside the producer function.
We **subscribe** to conent using : `observable.subscribe(subscriptionFunction, errorFunction, completeFunction)`, the function will be called every time the observer publishes a new value.

Observable = Producer Function + Subscriptors

To **complete** the observable : we call `observer.complete()` inside the observer function.

### Hot Vs Cold observables

Cold Observable : the producer is activated once the subscription has been created.
Hot Observable : the producer is emiting values outside the producer function. The subscriptor will get previous events on create and on every next event after that.

## Subjects

A type of observable.

We can **subscribe and emit** on the same object.

### Subject types

Behaviour subject :

- New subscriptions get last item emited
- Creator : Need an initial event

Replay subject :

- Creator :
  - We provide a number of events to be replayed to new subscribers
  - We can also provide a window of time in which items will be replayed
- New subscriptions get last parameterized items emited

Async subject :

- Requires `subject.complete()` to be called to start emiting values.

## Basic operators and marble diagrams

Operators : methods that allow to create another observable based on previous observable.
Operators are [pure functions](20210412165330).

Two types of operators :

- Statics : used to create observables
- Instance : works over exisisting operators, the majority of operators.
