---
id: 20211214143328
tags: []
references:
 - 20211214143245
---

# Paul Bricman

- [Website](https://paulbricman.com)
- [GitHub](https://github.com/paulbricman)
- [Email](paulbricman@protonmail.com)
