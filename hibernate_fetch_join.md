---
id: 20220512153731
tags: []
references:
 - 20210325170539
---

# Hibernate fetch join

In order to fetch childerns in the same query as the parent and avoid [n+1 problems](20220512142613) a `.fetchJoin()` in the query like `.leftJoin(...).fetchJoin()`, in this case we will have 1 parent for every children, we can append a `.distinct()` to have a single entry for every parent.
