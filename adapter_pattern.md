---
id: 20220506153420
tags: []
references:
 - 20210114174306
---

# Adapter pattern

**Without changing a class** that doesn't implement the desired interface, being able to **provide the desired interface**.

It allows incompatible interfaces to work together.

## When to use it

The adapter design pattern solves problems like:

- How can a class be reused that does not have an interface that a client requires?
- How can classes that have incompatible interfaces work together?
- How can an alternative interface be provided for a class?

## How to use it

- Define a separate `adapter` class that converts the (incompatible) interface of a class (`adaptee`) into another interface (`target`) clients require.
- Work through an adapter to work with (reuse) classes that do not have the required interface.

Check more in [Wikipedia](https://en.wikipedia.org/wiki/Adapter_pattern)
