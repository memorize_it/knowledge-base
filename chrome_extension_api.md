---
id: 20210415102935
tags: []
references: []
---
# Chrome Extension Api

- [Icon : Color/Grey/Change](https://developer.chrome.com/docs/extensions/reference/action/)
- [Run script on current window context](https://developer.chrome.com/docs/extensions/mv3/content_scripts/)
  - Script will run in an isolated context, sharing the DOM.
    - The script doesn't run on the extension context. We can communicate through [messages](https://developer.chrome.com/docs/extensions/mv3/messaging/).
    - The script doesn't run on the host context
  - Use static content script declarations in manifest.json for scripts that should be automatically run on a **well known set of pages**.
  - Use dynamic declarations when the host is not well known.
    - This feature is not yet fully supported. It is currently in dev and is also available in Chrome Canary.
  - We can use `window.postMessage` to fully communicate with the host.
- [Messaging between contetn_scripts and extension](https://developer.chrome.com/docs/extensions/mv3/messaging/)
