---
id: 20210716094527
tags: []
references:
 - 20210608121248
---

# Add resolution to screen

```bash
gtf 2560 1440 30 # Get the modline
xrandr --newmode "2560x1440"  114.37  2560 2648 2912 3264  1440 1441 1444 1460  -HSync +Vsync
xrandr --addmode HDMI2 "2560x1440" # Add mode to device
xrandr --output HDMI2 --mode "2560x1440" # Use mode
```
