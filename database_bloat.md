---
id: 20220120141102
tags: []
references:
 - 20210608121348
---

# Database bloat

Records that are not being cleaned by the autovacuum. This may occur when data is being updated faster than the autovacuum can delete old records.

If the [Bloat score](20220120141512) is high, this may affect database performance.
