---
id: 20210114174432
tags:
   - design_pattern
references: []
---
# Singleton

Limits the creation of a single instace of an object. And wrapp it inside a larger class or object that protects the private object verifying that a single instance will be created.

In #java we need to make the constructor private to achieve this. And then create get methods to recover it.

```java
public class SingleObject {

   //create an object of SingleObject
   private static SingleObject instance = new SingleObject();

   //make the constructor private so that this class cannot be
   //instantiated
   private SingleObject(){}

   //Get the only object available
   public static SingleObject getInstance(){
      return instance;
   }

   public void showMessage(){
      System.out.println("Hello World!");
   }
}
```
