---
id: 20210108111125
tags: []
references: []
---
# Oh my zsh setup

Documentation at [GitHub](https://github.com/ohmyzsh/ohmyzsh).

## Install zsh

```bash
sudo apt install zsh
```

## Install oh my zsh

```bash
curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh >> /tmp/zsh.sh
sh /tmp/zsh.sh
```

## Install theme

Clone theme from github :

```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

Add this to ~/.zshrc

```bash
ZSH_THEME="powerlevel10k/powerlevel10k"
```
