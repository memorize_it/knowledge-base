---
id: 20210712153753
tags: []
references:
 - 20210608121324
---

# HTTP

## Status codes

- [HTTP 401 Unauthorized](20210712153919) : Invalid credentials
- [HTTP 403 Forbidden](20210712153920) : Valid credentials but not enough permissions
