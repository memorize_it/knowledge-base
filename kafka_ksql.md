---
id: 20211126142106
tags: []
references:
 - 20211125093626
---

# KSql

- Based in SQL
- Exposes a REST API
- Two modes
  - Standalone : 1 client + 1 server and  same jvm
  - Client-server : n clients + n servers
- Very useful for small consumptions, use [kafka streams](20211125103644) for more advanced cases
