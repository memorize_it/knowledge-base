---
id: 20211207134225
tags: []
references:
  - 20211207133524
---

# The median

The value that separates the lower half from the lower half of a set.

See [Wikipedia](https://en.wikipedia.org/wiki/Median)

In order to calculate the median

```js
function findMedian(a) {
  const length = a.length;

  // Sort the array
  a.sort();

  // If the amount of elements is not pair, the element if the middle is the median
  if (length % 2 !== 0) {
    return a[length / 2];
  }

  // If the amount of elements is pair, the average of the two closest values should be retained
  return (a[Math.floor((length - 1) / 2)] + a[length / 2]) / 2;
}
```
