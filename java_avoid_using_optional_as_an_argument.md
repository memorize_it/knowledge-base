---
id: 20211214095052
tags: []
references:
 - 20210325170539
---

# Java avoid using Optional as an argument

The Java language authors think that **Optional was intended for use only as a return type**, as a way to convey that a method may or may not return a value.

Using Optional on the input side increases the work without increasing the value.

Without Optional parameter -> 2 possible inputs: null and not-null
With Optional parameter -> null, non-null-without-value, and non-null-with-value.

For more information you can check :

- this [stack overflow answer](https://stackoverflow.com/a/26328555) from [Brian Goetz](https://twitter.com/BrianGoetz)
- the [Sonar Rule](https://rules.sonarsource.com/java/RSPEC-3553)
