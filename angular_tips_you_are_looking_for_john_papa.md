---
id: 20210414101949
tags:
    - talk
    - angular
    - ngrx
    - rxjs
    - youtube
references: []
---
# Angular tips you are looking for John Papa

Watch it on [YouTube](https://www.youtube.com/watch?v=2ZFgcTOcnUg)

## RxJs

- How to manage multiple observables subscriptions :
  - How to avoid memory leaks
  - Use SubSync (an array of subscriptions) : unsubscribe on destroy, all at once

## State management

- When do we have CRUD models, we can automate NgRx code
  - [NgRx Data](https://ngrx.io/guide/data)

## Cloud services

- Scale Angular App
  - [JAM stack](20210414103545)
  - Store app on the cloud
    - API : Lambdas or Azure functions
    - Front : file storage
  - Avoid server maintenance and cost
