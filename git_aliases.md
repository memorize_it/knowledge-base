---
id: 20210108162524
tags: []
references: []
---
# Git aliases

Add this to ~/.gitconfig

```shell
[alias]
 lg = log --graph --oneline --decorate --all
 list = stash list
 co = checkout
 cob = checkout -b
 br = branch
 ci = !git add -A && git commit -m
 wip = commit -am "WIP"
 undo = reset HEAD~1 --mixed
 st = status
 puf = push -f
 cm = commit -m
 ame = commit --amend --no-edit
 back = !git co -b
 gd = !diff -- ':!package-lock.json' ':!yarn.lock'
 re = !sh -c \"git rebase -i HEAD~$1\"
[push]
 default = simple
[pull]
 rebase = true
```
