---
id: 20210108162425
tags: []
references: []
---

# Development

- [Computer science](20210608121335)
- [Git](20210608121259)
- [Linux](20210608121248)
- [Databases](20210608121348)
- [App development](20210608121324)
- [Types](20210716102102)
- [Networking](20210716172931)
- [Code Review](20220310085535)
- [Editor](20220316101453)
- [Problems](20220822172456)

## Raspberry pi

- [Raspberry Pi bridge](20200816104302)
- [Raspberry PI Routed wireless access point](20200816113204)
- [Raspberry PI Docker compose](20220225093940)

## Ruby

- [Install ruby](20210317113402)

## Java

- [Java](20210325170539)
- [Java Setup](20210108115151)

## Hardware

- [Drive vs Disk](20210621100248)

## Puzzles

- [Advent of code](20211207133524)

## Algorithms

- [Aho-Corasick](20220117175609)

## Shell

- [Read shell parameters after n position](20220125103741)

## Others

- [Chrome Extension development](20210711115735)
