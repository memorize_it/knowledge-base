---
id: 20210603112048
tags: []
references: []
---
# Java Object hash vs hashcode

Objects.hash(Object... values) : hash of a sequence of objects, e.g. when defining your own hashCode method and want a simply-coded hash for multiple values that make up the identity of your object.
Objects.hashCode(Object o) : hash of a single object, **without throwing** if the object is null.
Object::hashCode() : hash of a single object, and **will throw** an exception if the object is null.
