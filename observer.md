---
id: 20210114175729
tags:
    - design_pattern
references: []
---
# Observer

The consumer subscribes to the subject, usually providing a callback. And the subject notifies the consumer when the given action occurs.
