---
id: 20211126095028
tags: []
references:
 - 20211125093626
---

# Kafka Connect

- Take kafka records and put them in a data source
- Take recors from a data source and push them into kafka
- The integration is done with community connectors or custom connectors

- Sink data : kafka to another source
- Source data : another source to kafka

## How it works

- Two modes :
  - Standalone
    - For
      - Single process
      - Easy to start and test
    - Against
      - No fault tolerant
      - No scalable
  - Distributed
    - For
      - Fault tolerant
      - Scalable
    - Against
      - Many processes
      - Harder to deploy and maintain
      - Offsets are automatically managed

## Just config

- Default parameters :
  - Name
  - Connector.clkass
  - ...
- Custom parameters

## Custom connectors

- Config definition
- Interface
- Data model
- Task
- Deploy

## Api

```bash
connect-standalone worker.properties file-source.properties
```

## Properties

### Sink

```yaml
# Basic configuration for our connector
name=JdbcSinkConnector-cart
connector.class=io.confluent.connect.jdbc.JdbcSinkConnector
topics=cart-topic-2
tasks.max=2

# Postgres database
connection.user=postgres
connection.password=postgres
connection.url=jdbc:postgresql://localhost:5432/postgres

# We use avro converter for sink to database
value.converter=io.confluent.connect.avro.AvroConverter
value.converter.schema.registry.url=http://localhost:8081

# we want the primary key to be offset + partition
pk.mode=kafka

# default values
pk.fields=__connect_topic,__connect_partition,__connect_offset
fields.whitelist=name,price,date,user

# Auto configuration
auto.create=true
auto.evolve=true
```

### Worker

```yaml
# from more information, visit: http://docs.confluent.io/3.2.0/connect/userguide.html#common-worker-configs
bootstrap.servers=127.0.0.1:9092
key.converter=org.apache.kafka.connect.json.JsonConverter
key.converter.schemas.enable=false
value.converter=org.apache.kafka.connect.json.JsonConverter
value.converter.schemas.enable=false

# we always leave the internal key to JsonConverter
internal.key.converter=org.apache.kafka.connect.json.JsonConverter
internal.key.converter.schemas.enable=false
internal.value.converter=org.apache.kafka.connect.json.JsonConverter
internal.value.converter.schemas.enable=false

# Rest API
rest.port=8086
rest.host.name=127.0.0.1

# this config is only for standalone workers
offset.storage.file.filename=standalone.offsets
offset.flush.interval.ms=10000

# Setup connectors
plugin.path=/opt/landoop/connectors

# Read from the begginnig
auto.offset.reset=earliest
```
