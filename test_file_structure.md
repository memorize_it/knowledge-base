---
id: 20221221155035
tags: []
references:
 - 20210816102242
---

# Test file structure

In the tests, we can divide files as follos :

- Actions file -> User events (click, dismiss, scroll...)
- Asserts file -> Evaluate what is visible by the user
- Extractors files -> Extract elements from the dom
