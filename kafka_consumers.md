---
id: 20211125104304
tags: []
references:
 - 20211125093626
---

# Kafka Consumers

- reads messages in a given topic
- susbcribes to a given number of partitions inside the topic
- can be grouped in consumer groups

## Consummer groups

- allows to group consumers
- 1 consumer n partitions
- keeps track of current offset for every partition
- two consommer groups can consome the same message, but inside a consummer group a message will only be consommed once

## Cli

```bash
# kafka-console-consumer --bootstrap-server <BOOTSTRAP_SERVER> --topic <TOPIC_NAME>
kafka-console-consumer
kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic my_topic
```

## Java

This example is in plain java, Spring has an specific integration to make things easier.

```java
import org.apache.kafka.clients.consumer.*;

// Min properties
Properties properties = new Properties();
properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "group-1");
properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

// Create consumer
KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);

// Subscribe a topic
consumer.subscribe(Collections.singleton("cart-topic"));

// Poll data
while (true) {
    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
    if (!records.isEmpty()) {
        StreamSupport.stream(Spliterators
                .spliteratorUnknownSize(records.iterator(), 0), false)
                .forEach(
                        consumerRecord -> {
                            logger.info(consumerRecord.value());
                        }
                );
    }
}
```
