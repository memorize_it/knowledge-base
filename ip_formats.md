---
id: 20210716173000
tags: []
references:
 - 20210716172942
---

# IP formats

Check the [Wikipedia](https://en.wikipedia.org/wiki/IPv4#Addressing) article for more information.

IPv4 addresses may be represented in any notation expressing a 32-bit integer value.

For example, the quad-dotted IP address 192.0.2.235 represents the 32-bit decimal number 3221226219, which in hexadecimal format is 0xC00002EB.

- hex format : 0xC0.0x00.0x02.0xEB
- octal byte values : 0300.0000.0002.0353
