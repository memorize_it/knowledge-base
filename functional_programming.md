---
id: 20210413103144
tags: []
references: []
---
# Functional programming

A [programing paradigm](20210413103238).

Decompose a problem in a series of [pure functions](20210412165330). Then compose with the functions.

High order function, a function which :

- gets a **function as input**
- returns a **function as output**
- or both

Composing :

- a function which receives an array of functions
- returns a wrapper function that will call the input functions, in order, and pass trough the output.
- `compose(toLowerCase, trim)` = `toLowerCase(Trim(...))`

Piping :

- Like composing, but with reverse order
- `pipe(trim, toLowerCase)` = `toLowerCase(Trim(...))`

[Currying](20210413150128)
