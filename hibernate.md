---
id: 20220512153739
tags: []
references:
 - 20210325170539
---

# Hibernate

- [Hibernate advantages](20210325170141)
- [Hibernate concepts](20210325170726)
- [Hibernate entity state](20210325175709)
- [Hibernate annotations](20210329101547)
- [Hibernate entity association](20210329102038)
- [Hibernate entity loading](20210401150034)
- [Hibernate session flush](20210401150908)
- [Hibernate fetch join](20220512153731)
