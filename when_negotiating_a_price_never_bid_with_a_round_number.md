---
id: 20220506135953
tags: []
references: []
---

# When negotiating a price, never bid with a round number

Investors who offer “precise” bids for company shares yield better outcomes than those who offer round-number bids, according to [research by Petri Hukkanen and Matti Keloharju](https://hbswk.hbs.edu/item/when-negotiating-a-price-never-bid-with-a-round-number).
