---
id: 20210325163813
tags: []
references: []
---
# Java memory zones

Two main categories :

- Memory zones created on JVM start
- Memory zones created per thread

Four types :

- Registers: when executing bytecode
- Stack
  - One per thread, local var, parameters, returns...
  - Only primitives and object references are stored
  - StackOverflowError if stack is too small
  - **OutOfMemoryError** if JVM has no mememory for a new Stack
- Heap
  - Shared on all the JVM
  - All the objects and arrays
  - Garbage collector
  - It can grow or be fixed on JVM startup
  - **OutOfMemoryError** if an object can't be created
- Method area
  - Shared by all threads
  - Class definitions, constructors, methods...
  - Like stack, only primitives or object references
  - Accessible by all threads, so you have to secure static variable access
- Code cache: compiled method code
