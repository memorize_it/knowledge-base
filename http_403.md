---
id: 20210712153920
tags: []
references:
 - 20210712153753
---

# HTTP 403 - Forbidden

See [RFC 7231](https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.3)

    The 403 (Forbidden) status code indicates that the server understood the request but refuses to authorize it.
