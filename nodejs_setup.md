---
id: 20210108112223
tags: []
references: []
---
# Node.js setup

I use NVM. Check the [GitHub](https://github.com/nvm-sh/nvm)

```bash
curl -fsSL https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh  >> /tmp/nvm.sh
sh /tmp/nvm.sh
echo 'export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm' >> ~/.zshrc 
source ~/.zshrc
nvm install node
node --version
```

- [Install global npm packages](20210108113301)
