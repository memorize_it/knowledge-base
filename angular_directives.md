---
id: 20210412150245
tags:
    - angular
references: []
---
# Angular directives

From [A Practical Guide to Angular Directives](https://www.sitepoint.com/practical-guide-angular-directives/)

## Types

- Components :
  - directives with templates. Under the hood, they use the directive API and give us a cleaner way to define them.
- Attribute directives :
  - manipulate the DOM by changing its behavior and appearance.
  - conditional style to elements, show or hide elements or dynamically change the behavior of a component according to a changing property.
  - built in : ngClass, ngStyle
  - example :

    ```js
    import {Directive, ElementRef} from '@angular/core';

    @Directive({
        selector:'[my-error]'
    })

    export class MyErrorDirective{
        constructor(elr:ElementRef){
            elr.nativeElement.style.background='red';
        }
    }
    ```

- Structural directives
  - create and destroy DOM elements.
  - built in : ngIf, ngFor, ngSwitch
  - example :

    ```js
    import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

    @Directive({
        selector: '[myCustomIf]'
    })

    export class MyCustomIfDirective {

        constructor(
            private templateRef: TemplateRef<any>,
            private viewContainer: ViewContainerRef) { }

        @Input() set myCustomIf(condition: boolean) {
            if (condition) {
                this.viewContainer.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainer.clear();
            }
        }
    }
    ```
