---
id: 20220125103741
tags:
    - snippet
references:
 - 20210108162425
---

# Read shell parameters after n position

```sh
for i in $(seq 2 $#);
do
    N_PARAM_VALUE="${@:$i:1}"
done
```
