---
id: 20220128093712
tags: []
references:
 - 20210527094612
---

# Alacritty

- [GIthub Repository](https://github.com/alacritty/alacritty)

## Shortcuts

- Copy-Paste : `Ctrl + Shift + v`
