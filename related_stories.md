---
id: 20211129090727
tags: []
references: []
---

# Related stories

When creating a related ticket (sub-task or others).

In addition to add a description of what is to be done, explain when it has to be done (ec: after x toggle has been up for 10 days, 15 days after y story has been merged...)
