---
id: 20210608121259
tags: []
references:
 - 20210108162425
---

# Git

- [Initialize git](20210108155213)
- [Git aliases](20210108162524)
- [Git credential store](20210317115025)
- [Change git commit author](20210628101345)
- [Checkout = Restore + Switch](20210805093957)
