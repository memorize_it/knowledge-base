---
id: 20210415095019
tags: []
references: []
---
# Chrome Extension CD

Sources :

- [Circle CI - How to Continuously Deploy a Chrome Extension](https://circleci.com/blog/continuously-deploy-a-chrome-extension/)
- [Chrome Developers - Using the Chrome Web Store Publish API](https://developer.chrome.com/docs/webstore/using_webstore_api/)

## Preparation

Enable the API for your project in the [Google Developers Console](https://console.developers.google.com).

For that :

- APIs & auth > Chrome Web Store API > **ON**
- Credentials > Client Id and Client Secret
  - CREATE CREDENTIALS, select OAuth client ID and select **Desktop app** under Application type.
- Accepts permissions : `https://accounts.google.com/o/oauth2/auth?response_type=code&scope=https://www.googleapis.com/auth/chromewebstore&client_id=$CLIENT_ID&redirect_uri=urn:ietf:wg:oauth:2.0:oob`
- Get the $CODE
- Now to get an access token : `url "https://accounts.google.com/o/oauth2/token" -d "client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&code=$CODE&grant_type=authorization_code&redirect_uri=urn:ietf:wg:oauth:2.0:oob"`
- The access token will be in JSON format
- We need to recover the `refresh_token` property.
- Get the APP_ID from the [Chrome Webstore](https://chrome.google.com/webstore/category/extensions)

## Env variables

We need to create four environment variables :

- CLIENT_ID
- CLIENT_SECRET
- REFRESH_TOKEN
- APP_ID

## Using the API

Geting a fresh token

```shell
ACCESS_TOKEN=$(curl "https://accounts.google.com/o/oauth2/token" -d "client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&refresh_token=${REFRESH_TOKEN}&grant_type=refresh_token&redirect_uri=urn:ietf:wg:oauth:2.0:oob" | jq -r .access_token)
```

Create the APP.

```shell
curl \
-H "Authorization: Bearer $ACCESS_TOKEN"  \
-H "x-goog-api-version: 2" \
-X POST \
-T $FILE_NAME \
-v \
https://www.googleapis.com/upload/chromewebstore/v1.1/items
```

Update the app.

```shell
curl \
-H "Authorization: Bearer $ACCESS_TOKEN"  \
-H "x-goog-api-version: 2" \
-X PUT \
-T $FILE_NAME \
-v \
https://www.googleapis.com/upload/chromewebstore/v1.1/items/$APP_ID
```

Publishing an item to the public.

```shell
curl \
-H "Authorization: Bearer $ACCESS_TOKEN"  \
-H "x-goog-api-version: 2" \
-H "Content-Length: 0" \
-X POST \
-v \
https://www.googleapis.com/chromewebstore/v1.1/items/$APP_ID/publish
```
