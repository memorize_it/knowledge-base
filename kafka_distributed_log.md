---
id: 20211125103915
tags: []
references:
 - 20211125093626
---

# Kafka Distributed log

- Data are stored in disk
- In a sequential manner, oldest to most recent

## What is in the log
  
- Offset : position in the log
- Timestamps : insertion data
- Key (optional - byte array)
- Value (byte array)
- Headers (key-value)
