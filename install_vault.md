---
id: 20210614171849
tags: []
references:
 - 20210608121248
---

# Install Vault

Source [Vault documentation](https://www.vaultproject.io/downloads).

```bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install vault
```
