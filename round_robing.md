---
id: 20211125104809
tags: []
references:
 - 20211125104038
---

# Round Robin

See [Wikipedia](https://en.wikipedia.org/wiki/Round-robin_scheduling)

Each person takes an equal share of something in turn.
