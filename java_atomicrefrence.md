---
id: 20220120114435
tags: []
references:
 - 20220120113043
---

# Java AtomicRefrence

When a value should be shared between multiple threads, [`AtomicReference`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicReference.html) allows having a single reference to an object.
