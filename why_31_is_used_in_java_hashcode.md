---
id: 20211216120449
tags: []
references:
 - 20211216120409
---

# Why 31 is used in Java HashCode

The answer is based on [Joshua Bloch](20211216120744)'s argumentation in [Effective Java](https://www.oreilly.com/library/view/effective-java/9780134686097/)

The value 31 was chosen because :

## It is an odd prime

If it were even and the multiplication overflowed, information would be lost, as multiplication by 2 is equivalent to shifting.

The advantage of using a prime is less clear, but it is traditional.

## Improved multiplicaiton

A nice property of 31 is that the multiplication can be replaced by a shift and a subtraction for better performance: `31 * i == (i << 5) - i`.

Modern VMs do this sort of optimization automatically.
