---
id: 20211207134227
tags: []
references:
 - 20211207133524
---

# Absolute deviation

Given a set, the absolute difference between an element of the set and a given point.

See [Wikipedia](https://en.wikipedia.org/wiki/Deviation_(statistics)#Unsigned_or_absolute_deviation)
