---
id: 20210816102351
tags: []
references:
 - 20210816102242
---

# Avoid nested tests

Hints after reading [Kent C. Dodds's](https://kentcdodds.com/) article [Avoid nesting when you are testing](https://kentcdodds.com/blog/avoid-nesting-when-youre-testing).

## Test setups

- Only refactor code to avoid duplication when the setup reduces test readability.
- If too much code is being reused, create setup functions that expose variables needed for the tests.

## Grouping tests

- Instead of separate describes, we can use different suffixed files.
- Need for common code ? Create a `helpers` file.
