---
id: 20210329143915
tags: []
references: []
---
# Query performance

How to measure query performance :

- Using a timer
- Using logical vs physical reads

MySQL :

```sql
SET STATISTICS TIME ON;
SET STATISTICS IO ON;
```

PostreSQL:

```sql
EXPLAIN ANALYZE
```
