---
id: 20210413145518
tags:
    - talk
    - redux
    - youtube
references: []
---
# Practical Advanced Redux - Boris Dinkevich

Watch it on [YouTube](https://www.youtube.com/watch?v=Gjiu7Lgdg3s)

- Middlewares are applied :
  - After a dispatch
  - Before an action reaches the store
- Basic middleware sintax
  - Uses [currying](20210413150128)

    ```js
    const middleware = ({getState, dispatch}) => next => action => {
        ...
        next(action);
    }

    export default middleware:
    ```

  - Pass the middleware on the store creation using applyMiddleware.
- Types of middlewares :
  - All actions
  - All with flag : read meta
  - One type of action : read type
- Allows to just trigger actions
