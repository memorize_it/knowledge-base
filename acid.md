---
id: 20210329154501
tags: []
references: []
---
# ACID

- Atomicity : one transaction is a single unit
- Consistency : from valid state to valid state, no half transaction applied
- Isolation : concurrent transactions must leave the database in the same state as if the tranactions were sequential
- Durability : once a transactions is commited it will stay even if system crashes
