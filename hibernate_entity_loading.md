---
id: 20210401150034
tags: []
references: []
---
# Hibernate entity loading

[Baeldung](https://www.baeldung.com/hibernate-lazy-eager-loading)

Data loading can be eager and lazy.

- Eager : Data will be always loaded.
  - Cannot be overridden, tying your application to the global fetch plan
- Lazy : Data will only be loaded into memory on property access
  - At query time, we may override the global fetch plan.

For **lazy** loading a **proxy object** is used and a separate SQL query is fired.

## Usage

```java
@Entity
@Table(name = "USER")
public class UserLazy implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    private Long userId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<OrderDetail> orderDetail = new HashSet();

    // standard setters and getters
    // also override equals and hashcode

}

@Entity
@Table (name = "USER_ORDER")
public class OrderDetail implements Serializable {
    
    @Id
    @GeneratedValue
    @Column(name="ORDER_ID")
    private Long orderId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="USER_ID")
    private UserLazy user;

    // standard setters and getters
    // also override equals and hashcode

}
