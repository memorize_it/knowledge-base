---
id: 20210311080507
tags: []
references: []
---
# MOOC : React Native

Using the MOOC : [CS50's Mobile App Development with React Native](https://learning.edx.org/course/course-v1:HarvardX+CS50M+Mobile/).

## React, Props, State

### State

Internally managed configuration of the component.

Can only be updated using `this.setState()`. Which updates asynchronously.

We initialize state in the constructor :

```javascript
class App extends React.Component {
    constructor (props){
        super(props);
        this.state= {
            count: 0
        };
    }
}
```

### Props

Come in from the jsx properties.

## React native

Two threads, one for UI, one for JS. Blocking JS won't block UI.

No browser API (canvas, style...)

No global components, need to import from react-native :

- div > View
- span > Text
- button > Button
  - onClick > onPress
- List > ScrollView
- ...

Style is managed with Objects. Flexbox available. Lengths are unitless numbers.

We add styles on style={{}}.

We ca manage global styles with :

```js
export const styles = StyleScheet.create({
    appContainer: {...},
    todoLists: {...}
})
```

Component life cycle : Mount -> Update (Loop) -> Unmount

- Mount :
  - Constructor
  - Render -> Return a node
  - Call componentDidMount
