---
id: 20220121145106
tags: []
references:
 - 20220120115017
---

# Double checked locking

Patter that can be used in multi thread environments when we need to ensure that shared resources are only initialized once.

```java
public class Car {
    AtomicReference<Engine> threadSafeEngine;

    public Engine getEngine(){
        Engine e = threadSafeEngine.get();

        if(e != null){
            return e;
        }

        return produceEngine();
    }

    private synchronized Engine produceEngine(){
        Engine e = threadSafeEngine.get();

        if(e != null){
            return e;
        }

        e = EngineFactory.build();
        threadSafeEngine.set(e);
        return e;
    }
}
```
