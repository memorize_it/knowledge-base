---
id: 20220316092722
tags: []
references:
 - 20210608121324
---

# CORS

## Theory

- **HTTP Headers** to manage source exchange between **different origins**.
- Prevents code execution on a site from an unknown source. In theory.
- HTTP Request and Fetch API uses CORS by default in modern browsers.
- HTML doesn't user cors. Usually !
- The destination server should return `Access-Control-Allow-Origin: *`
  - Or set a **single** site instead of `*`
- **Options** requests can be sent to ensure server supports headers
  - The server will return `Access-Control-Allow-Origin` header (among other things)
- We can provide credentials for the CORS

## What we have to do

- Wildcard on public servers
- Verify the origin of the request
- CORS are not an authentication method

## CORS and security

**NO**. We can override origin, so it does not add a security layer.

## CORS in Spring

If configuration is defined it will override controller configuration.
