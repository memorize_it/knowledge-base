---
id: 20210608121348
tags: []
references:
 - 20210108162425
---

# Databases

- [Normalized vs Denormalized data](20210628111414)
- [Database bloat](20220120141102)
- [Zero downtime database migration](20220505145245)

## SQL

- [Inner vs Outer Join](20210114194433)
- [SQL Performance](20210325172540)
- [SQL query performance](20210329143915)
- [SQL Why to index foreign keys](20220601101135)

### PostgreSQL

- [Pgbadger](20210616145922)
- [Postgresql index types](20210402085527)
- [Postgresql pretty print](20210721150115)

## Tips

- [Migrate sql db entries without overriding latest](20220131144121)
- [The n+1 problem](20220512142613)
