---
id: 20210108115151
tags: []
references: []
---

# Java setup

```shell
sudo apt-get install default-jdk
```

## Manage java envs

Use [jEnv](20210804094626).

## Open JDK Builds

- [Azul Zulu](https://www.azul.com/downloads/?package=jdk)
