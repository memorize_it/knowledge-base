---
id: 20210413140109
tags:
    - talk
    - redux
    - youtube
references: []
---
# Advanced Redux: Design Patterns and Practices - Nir Kaufman

Watch it on [YouTube](https://www.youtube.com/watch?v=5gl3cCB_26M)

## Tips

- Program with actions.
- Three action categories :
  - Command : ask for something, start the process
  - Event : something happend
  - Document : write data to the state
- Prefix every slice, so that action types refer to the slice

    ```js
    export const BOOKS = "[BOOKS]";
    export const GET_BOOKS = `${BOOKS} Get`;
    export const SET_BOOKS = `${BOOKS} Set`;
    ```

- The hard part : how do we convert command actions into document actions ?
  - We use Action processing patterns in our Middleware (reducer)
  - Action routing patterns
    - Filter
    - Map
      - Trigger action based on payload
    - Split
      - Catch an action and dispatch N actions.
  - Multi middleware
    - Aggregate
    - Compose
  - Action transformation patterns
    - Enrich
    - Normalize
  - Translate
- Two types of middleware :
  - Core : generic and reusable (api, logging), won't trigger Application middlewares
  - Application : describe a specific flow (BL specific), may trigger Core middlewares
