---
id: 20210413101543
tags:
    - tutorial
    - redux
    - youtube
references: []
---
# Redux Tutorial - Learn Redux from Scratch

Watch it on [YouTube](https://www.youtube.com/watch?v=poQXNp9ItL4)

## Functional programming

[Functional programming](20210413103144)

## Fundamentals of redux

[Redux](20210412153110)

## Build redux from scratch

## Debugging redux

## Writing clean code

## Redux store

## Middleware

## Calling APIs

## Testing Redux app

## Integration with React
