---
id: 20210114190721
tags:
    - design_pattern
references: []
---
# Strategy

- We define an interface, and then make all the different implementations.
- Then an object is instantiated, with the interface as a parameter.
- Then on every call we define which implementation is going to be used, based on a parameter, the context...
