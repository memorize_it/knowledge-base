---
id: 20210621100248
tags: []
references:
 - 20210108162425
---

# Drive vs Disk

- Disks store informatiokn on a mechanical disk, **slower** for random reads
- Drives (like SSD) store information on semi-conductors, **faster** for random reads

- Disk have one head, this **performs well on sequential access**
- Drive have many flash chips, allowing parallel access

- Drives has low write speed, this is mitigated with a volatile RAM
