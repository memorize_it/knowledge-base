---
id: 20210930142716
tags: []
references:
 - 20210723143307
---

# Inspiring websites

- [Hacker news client with keyboard navigation](20210930142745)
- [Blog with post preview on hover and minimalist design](20220406094005)
- [Natural earth color palette of MusiCo](20220930134811)
