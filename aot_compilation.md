---
id: 20210412145302
tags:
    - angular
references: []
---
# AOT Compilation

Ahead-Of-Time compilation.

Compile on build instead of JIT.

Advantages :

- Faster rendering
- Fewer AJAX requests : html and css are shipped with app
- Minimizing errors : build compilation
- Better security : less files loaded on runtime
