---
id: 20221013145032
tags: []
references:
 - 20210608121324
---

# Duck typing

    If it walks like a duck and it quacks like a duck, then it must be a duck

## What does this mean ?

In development languages (like javascript), it's the actual opject properties that determine if it fits the requirements,  rather than checking asserting the object type.

```javascript
let object = {length:5};
object.length //5
let array = [1,2,3,4,5];
array.length //5
```
