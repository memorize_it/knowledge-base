---
id: 20210402090516
tags: []
references: []
---
# Spring data entity loading

[Baeldung](https://www.baeldung.com/spring-data-jpa-named-entity-graphs)

See also [Hibernate entity loading](20210401150034)

First, we can use JPA's @NamedEntityGraph annotation directly on our Item entity:

```java
@Entity
@NamedEntityGraph(name = "Item.characteristics",
    attributeNodes = @NamedAttributeNode("characteristics")
)
public class Item {
 //...
}
```

And then, we can attach the @EntityGraph annotation to one of our repository methods:

```java
public interface ItemRepository extends JpaRepository<Item, Long> {

    @EntityGraph(value = "Item.characteristics")
    Item findByName(String name);
}
```

The default value of the type argument of the @EntityGraph annotation is EntityGraphType.FETCH.

- EAGER for defined nodes, LAZY otherwise.
- Will override Hibernate defaults.

We can define the **entity graph directly on the repository, overriding** the entity defaults.

```java
public interface CharacteristicsRepository 
  extends JpaRepository<Characteristic, Long> {
    
    @EntityGraph(attributePaths = {"item"})
    Characteristic findByType(String type);    
}
```

This will load the item property of the **Characteristic entity eagerly**, even though our entity declares a lazy-loading strategy for this property.
