---
id: 20210412144416
tags:
    - angular
references: []
---
# Angular life cycle hooks

Component event listeners, from creation to destruction.

- ngOnChanges : called when input properties of component changes.
- ngOnInit: called once, after ngOnChanges hook.
- ngDoCheck : after ngOnChanges and ngOnInit. Used to detect changes that can't be detected by Angular.
- ngAfterContentInit : after first ngDoCheck. After the content is projected inside the component.
- ngAfterContentInitChecked
- ngAfterView : after view is initialized.
- ngAfterViewChecked.
- ngOnDestroy: on component destroy.
