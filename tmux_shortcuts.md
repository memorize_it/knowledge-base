---
id: 20210622092745
tags: []
references: []
---

# Tmux shortcuts

- Create new tab `Ctrl+a c`
- Split vertical `Ctrl+a /`
- Split horizontal `Ctrl+a -`
