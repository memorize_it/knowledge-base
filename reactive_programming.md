---
id: 20220316141406
tags: []
references:
 - 20210608121324
---

# Reactive programming

- Event driven
- Elastic
- Resilient
- Responsive

## Reactive Stream specification

- Publisher
- Subscriber
- Subscription
- Processor

## Event loop

Receive request, manage the request in the event loop and trigger events, providing callback points.
Do something else.
Manage responses in the event loop.
