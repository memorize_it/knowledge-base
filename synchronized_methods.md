---
id: 20220121142159
tags: []
references:
  - 20220120115017
---

# Synchronized methods

A synchronized method can only be executed once at a time by all the threads using the same instance of an object.

```txt
When a thread invokes a synchronized method, it automatically acquires the intrinsic lock for that method's object and releases it when the method returns.
```

More information in the [Oracle Java Tutorials](https://docs.oracle.com/javase/tutorial/essential/concurrency/locksync.html)
