---
id: 20210114190857
tags:
    - design_pattern
references: []
---

# Facade

Describes a higher-level interface that makes the subsystem easier to use, it will instantiate several methods that implement te given interface and then perform a series of operations on one of them, based on input.

```java
public interface Franchise {
    public void Option();
    public void Cost();
}

public class KFC implements Franchise {
    public void Option() {
        System.out.println("KFC");
    }
    public void Cost() {
        System.out.println("Rs 1,00,00,000");
    }
}

public class McDonalds implements Franchise {
    public void Option() {
        System.out.println("McDonalds");
    }
    public void Cost() {
        System.out.println("Rs 90,00,000");
    }
}

public class FranchiseServiceReg {
    private Franchise KFC;
    private Franchise McDonalds;

    public FranchiseServiceReg(){
        KFC = new KFC();
        McDonalds = new McDonalds();
    }

    public void BuyKFCFranchise(){
        KFC.Option();
        KFC.Cost();
    }

    public void BuyMcDonaldsFranchise(){
        McDonalds.Option();
        McDonalds.Cost();
    }
}
```
