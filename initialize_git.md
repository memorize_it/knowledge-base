---
id: 20210108155213
tags: []
references: []
---
# Initialize git

## User

```bash
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

## Ssh

[Generate ssh keys](20210108155327).

Add ssh key to Git server.

Run

```bash
ssh -T git@gitlab.com
```
