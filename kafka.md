---
id: 20211125093626
tags: []
references: []
---

# Kafka

## Introduction

### History

- 2009 #LinkedIn : bus for application-database interaction
- 2011 Open Source
- Part of the #Apache fondation
- 2014 Creation of [#Confluent](https://www.confluent.io)
  - [Jay Kreps](https://twitter.com/jaykreps) & [Neha Narkhede](https://twitter.com/nehanarkhede) & [Jun Rao](https://twitter.com/junrao)
- 2019 Confluent raises 2.5 billions (23/01/19)

### What is Kafka ?

- Message oriented middleware
- Publish and subscribe to streams of records
- Fast
- Scalable
- Polyglot : can be used with many languages
- [Distributed log](20211125103915)

### Downsides

- Cluster management
- Topology management : how to strucutre content
- Late event mnagement : when aggregation is been done, how to manage late events

### Alternatives

- Rabbit Mq / Acctive Mq
- GCP Pub/Sub
- Aws SQS
- Amazon Kinesis

### Providers

- Confluent
- Amazon MSK (Kafka managed by AWS)

## Core concepts

### Actors

- [Kafka Broker](20211125105813)
- [Producers](20211125103337)
- [Consumers](20211125104304)
- [Stream processors](20211125103644)
- [Topics](20211125104038)
- Connectors : interfaces with other systems, like storage systems
  - Produces a message to kafka when modifications are done in other systems

### Antripatterns

- Prefers lots [small records](20211126155946) over few large records (< 100ko)
- Push files, we will provide urls and cosummers will manage data
- Batching of messages allows kafka to optimize message processing
- Single source of truth for data, kafka is not intended to be a data storage
- Use it as a database
- Don't filter messages, this means that you need different topics

### Concepts

- [Guarantees](20211125111933)
- [Rest API](20211125143406)
- [Schema Registry](20211125145701)
- [Connect](20211126095028)
- [KSql](20211126142106)
- Consumer lag : a metric to be monitored, the lag between producers and consumers.

## Cloud

- Confluent is the default
- Confluent cloud or platform

### Kafka EC2

- Install on VMs
- Scale with AWS
- Need to be managed

### MSK (AWS Manage Kafka)

- Not all Confluence services
- Not integrated with all AWS services
- Encrypt data at rest
- No authentication and authorization for topics
- Managed version upgrade

## Ops

- [Topic performance](20211126154846)
- [Disk performance](20211126155946)

### On premise

- Java :
  - G1GC Garbage collector
  - 6-8G heap max
  - Lot of RAM
- OS Tuning
  - TCP Tuning
  - File descriptor limit > 100k
- Zookeeper on a distict cluster/pod

## Docker

- [landoop/fast-data-dev](https://hub.docker.com/r/landoop/fast-data-dev): self contained kafka distribution
  - kafka : 9092
  - connect : 8083
  - zookeper : 2181
  - ui : 3030
