---
id: 20210401092130
tags: []
references: []
---
# Java lists

## Implementations

- ArrayList : not synchronized
  - Better if we append elements
- LinkedList : not synchronized
  - Better if we add in a random position
- Vector : thred safe
- Stack : Vector with LIFO
- CopyOnWriteArrayList : thread safe
