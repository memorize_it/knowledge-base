---
id: 20210402085527
tags: []
references: []
---
# PostreSQL index types

[PostreSQL Tutorial](https://www.postgresqltutorial.com/postgresql-indexes/postgresql-index-types/)

PostgreSQL has several index types:

- B-tree (Balanced tree) : used for comparissons
- Hash : used for equals
- GiST : geometric and full-text search
- SP-GiST : used for naturally clustered information (ip, gps, multimedia...)
- GIN : when multiple values on a single column
- BRIN : smaller than a b-tree, use on big tables
