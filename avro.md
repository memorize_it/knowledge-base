---
id: 20211125150234
tags: []
references:
 - 20211125145701
---

# Avro

File format.

## Advantages

- Has a schema, which can be documented, evolve...
- Data is typed
  - null
  - boolean
  - int, long, float
  - string
  - enum, array, map, union
- Can be compressed

## Schema

- Composed by records
  - Name
  - Namespace
  - Doc
  - Aliases
  - Fields : array of fields
    - Name, doc, type, default
